/*

TEST
Checking the elements

*/

#include "wound.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept> 
#include <math.h> 
#include <string>
#include <stdlib.h>     /* srand, rand */
#include <time.h>    

#include <Eigen/Dense> 
using namespace Eigen;

double frand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int main(int argc, char *argv[])
{
	/* initialize random seed: */
	srand (time(NULL));
	  
	std::cout<<"\nTest: Derivatives \n";
	// check that global derivatives are good to go
	double dt = 0.25; // global time step in HOURS
	double local_dt = 0.25; // local time step in fraction

	
	//---------------------------------//
	// GLOBAL PARAMETERS
	//
	// for normalization
	double rho_phys = 100.; // [cells/mm^3]
	double c_max = 1.0e-4; // [g/mm3] from tgf beta review, 5e-5g/mm3 was good for tissues
	//
	double k0 = 0.05; // [MPa] neo hookean
	double k2c = 0.048; // nonlinear exponential collagen
	double k2f = 0.048; // nonlinear exponential fibronectin
	double t_rho = 4.5e-6; // [MPa] force of fibroblasts
	double t_rho_c = 4.5e-6; // [MPa] force of myofibroblasts enhanced by chemical
	double K_t_c = 1e-5; // saturation of chemical on force
	double D_rhorho = 0.0833; // [mm^2/hr] diffusion of cells
	double D_rhoc = 1.66e-4; // [mm^5/g/hr] diffusion of chemotactic gradient
	double D_cc = 0.1; // [mm^2/hr] diffusion of chemical
	double p_rho = 0.034 ; // [1/hr] production of fibroblasts naturally
	double p_rho_c = 0.001; // [1/hr] production enhanced by the chem
	double p_rho_theta = 0.001; // [1/hr] mechanosensing proliferation
	double K_rho_c= 1e-5; // saturation of cell production by chemical
	double K_rho_rho = 10000; // saturation of cell by cell
	double d_rho = 0.0068 ;// [1/hr] decay of cells
	double vartheta_e = 2.4; // physiological state of area stretch
	double gamma_theta = 5; // sensitivity of heviside function
	double p_c_rho = 9e-19;// [1/hr] production of C by single cell
	double p_c_thetaE = 3e-18; // coupling of elastic and chemical
	double K_c_c = 9e-12;// saturation of chem by chem
	double d_c = 0.01; //[1/hr] decay of chemical
	//
	std::vector<double> global_parameters = {k0,k2c,k2f,t_rho,t_rho_c,K_t_c,D_rhorho,D_rhoc,D_cc,p_rho,p_rho_c,p_rho_theta,K_rho_c,K_rho_rho,d_rho, vartheta_e,gamma_theta,p_c_rho,p_c_thetaE,K_c_c,d_c};
	//
	//---------------------------------//

	//---------------------------------//
	// LOCAL PARAMTERES
	//---------------------------------//
	//
	// fiber alignment
	double tau_omega_c = 5.0; // time constant for angular reorientation of collagen in [hr]
	double tau_omega_f = 5.0; // time constant for angular reorientation of fibronectin [hr]
	//
	// dispersion parameter
	double tau_kappa_c   = 0.5; // time constant for dispersion function [hr]
	double gamma_kappa_c = 2.0; // exponent of the principal stretch ratio for kappa evolution [-]
	double tau_kappa_f   = 0.5; // time constant for dispersion function [hr]
	double gamma_kappa_f = 2.0; // exponent of the principal stretch ratio for kappa evolution [-]
	// 
	// permanent contracture/growth
	double tau_lamdaP_a = 0.5; // time constant for direction a in [hr]
	double tau_lamdaP_s = 0.5; // time constant for direction s in [hr]
	//
	double thick_0  =  4.0; // reference thickness in [mm]
	double phic_vol =  0.6; // volume of collagen associated to healthy collagen fraction, for phic = 1, phic_vol = 0.6 (or 60%)
	double phif_vol =  0.01; // volume ot fibronectin associated to healthy state.
	std::vector<double> local_parameters = {tau_omega_c,tau_omega_f,tau_kappa_c,gamma_kappa_c,tau_kappa_f,gamma_kappa_f, tau_lamdaP_a, tau_lamdaP_s, thick_0, phic_vol, phif_vol};
	//
	// other local stuff
	double PIE = 3.14159;
	//---------------------------------//
	
	//-------------------------------------------------------------------------------//	

	// Some pre processing or variable initialization
	// nodal positions and fields at the nodes
	std::vector<Vector2d> node_X = {Vector2d(0.,0.),Vector2d(1.,0.),Vector2d(1.,1.),Vector2d(0.,1.)};
	std::vector<Vector2d> node_x = {Vector2d(0.,0.),frand(0.9,1.1)*Vector2d(1.,0.),frand(0.9,1.1)*Vector2d(1.,1.),frand(0.9,1.1)*Vector2d(0.,1.)};
	double rho_min = frand(0,1)*100;
	double c_init = frand(0,1)*c_max;
	std::vector<double> node_rho_0 = {rho_min,rho_min,rho_min,rho_min};
	std::vector<double> node_rho = {rho_min,rho_min,rho_min,rho_min};
	std::vector<double> node_c_0 = {c_init,c_init,c_init,c_init};
	std::vector<double> node_c = {c_init,c_init,c_init,c_init};
	std::vector<int> elem = {0,1,2,3};
	// compute the vector of jacobians
	std::vector<Matrix2d> ip_Jac = evalJacobian(node_X);
	// the integration point variables
	double phic0_init = frand(0,1)*1.;
	double phif0_init = frand(0,1)*(1.-phic0_init);
	double kc0_init = 0.05;
	double kf0_init = 0.01;
	double a0cx_init = frand(0,1);
	double a0cy_init = sqrt(1-a0cx_init*a0cx_init);
	Vector2d a0c_init = Vector2d(a0cx_init,a0cy_init);
	double thetacf = frand(0.0,1.0);
	Matrix2d Rot_thetacf; Rot_thetacf<<cos(thetacf),-sin(thetacf),sin(thetacf),cos(thetacf);
	Vector2d a0f_init = Rot_thetacf*a0c_init;
	double kappac0_init = frand(0,1)*0.5;
	double kappaf0_init = frand(0,1)*0.5;
	Vector2d lamdaP0_init;lamdaP0_init<<1,1; //= Vector2d(frand(0.8,1.2)*1,frand(0.8,1.2)*1);
	//
	std::vector<double> ip_phic_0 = {phic0_init,phic0_init,phic0_init,phic0_init};	
	std::vector<double> ip_phic = {phic0_init,phic0_init,phic0_init,phic0_init};		
	//
	std::vector<double> ip_phif_0 = {phif0_init,phif0_init,phif0_init,phif0_init};	
	std::vector<double> ip_phif = {phif0_init,phif0_init,phif0_init,phif0_init};		
	//
	std::vector<double> ip_kc_0 = {kc0_init,kc0_init,kc0_init,kc0_init};	
	std::vector<double> ip_kc = {kc0_init,kc0_init,kc0_init,kc0_init};		
	//
	std::vector<double> ip_kf_0 = {kf0_init,kf0_init,kf0_init,kf0_init};	
	std::vector<double> ip_kf = {kf0_init,kf0_init,kf0_init,kf0_init};		
	//
	std::vector<Vector2d> ip_a0c_0 = {a0c_init,a0c_init,a0c_init,a0c_init};
	std::vector<Vector2d> ip_a0c = {a0c_init,a0c_init,a0c_init,a0c_init};
	//
	std::vector<Vector2d> ip_a0f_0 = {a0f_init,a0f_init,a0f_init,a0f_init};
	std::vector<Vector2d> ip_a0f = {a0f_init,a0f_init,a0f_init,a0f_init};
	//
	std::vector<double> ip_kappac_0 = {kappac0_init,kappac0_init,kappac0_init,kappac0_init};
	std::vector<double> ip_kappac = {kappac0_init,kappac0_init,kappac0_init,kappac0_init};		
	//
	std::vector<double> ip_kappaf_0 = {kappaf0_init,kappaf0_init,kappaf0_init,kappaf0_init};
	std::vector<double> ip_kappaf = {kappaf0_init,kappaf0_init,kappaf0_init,kappaf0_init};		
	//
	std::vector<Vector2d> ip_lamdaP_0 = {lamdaP0_init,lamdaP0_init,lamdaP0_init,lamdaP0_init};
	std::vector<Vector2d> ip_lamdaP = {lamdaP0_init,lamdaP0_init,lamdaP0_init,lamdaP0_init};

	//---------------------------------//
	// RESULTS
	//---------------------------------//

	// create space for the residuals and tangents
	VectorXd Re_x(8);
	MatrixXd Ke_x_x(8,8); MatrixXd Ke_x_rho(8,4);
	MatrixXd Ke_x_c(8,4);
	VectorXd Re_rho(4);
	MatrixXd Ke_rho_x(4,8); MatrixXd Ke_rho_rho(4,4); MatrixXd Ke_rho_c(4,4);
	VectorXd Re_c(4);
	MatrixXd Ke_c_x(4,8); MatrixXd Ke_c_rho(4,4); MatrixXd Ke_c_c(4,4);
	// eval the wound function
	evalWound(dt, local_dt, ip_Jac, global_parameters, local_parameters,
			  node_rho_0,node_c_0, ip_phic_0, ip_kc_0, ip_a0c_0, ip_kappac_0, ip_phif_0,  ip_kf_0,  ip_a0f_0,  ip_kappaf_0, ip_lamdaP_0, 
			  node_rho,   node_c,  ip_phic,  ip_kc,  ip_a0c,  ip_kappac, ip_phif,  ip_kf,  ip_a0f,  ip_kappaf, ip_lamdaP, node_x, 
	  		  Re_x, Ke_x_x, Ke_x_rho, Ke_x_c, Re_rho, Ke_rho_x,  Ke_rho_rho, Ke_rho_c, Re_c, Ke_c_x, Ke_c_rho, Ke_c_c);
			
	// compute the tangents numerically
	double epsilon = 1e-8;
	VectorXd Re_x_p(8);
	VectorXd Re_x_m(8);
	VectorXd Re_rho_p(4);
	VectorXd Re_rho_m(4);
	VectorXd Re_c_p(4);
	VectorXd Re_c_m(4);
	MatrixXd Ke_x_x_num(8,8); MatrixXd Ke_x_rho_num(8,4);MatrixXd Ke_x_c_num(8,4);
	MatrixXd Ke_rho_x_num(4,8); MatrixXd Ke_rho_rho_num(4,4); MatrixXd Ke_rho_c_num(4,4);
	MatrixXd Ke_c_x_num(4,8); MatrixXd Ke_c_rho_num(4,4); MatrixXd Ke_c_c_num(4,4);

	// first move the displacements one by one
	std::vector<Vector2d> node_x_p = node_x;
	std::vector<Vector2d> node_x_m = node_x;
	// then you'll move the rho
	std::vector<double> node_rho_p = node_rho;
	std::vector<double> node_rho_m = node_rho;
	// and then you'll move the c
	std::vector<double> node_c_p = node_c;
	std::vector<double> node_c_m = node_c;
	for(int nodei=0;nodei<4;nodei++){
		for(int coordi=0;coordi<2;coordi++){
			node_x_p[nodei](coordi) += epsilon;
			node_x_m[nodei](coordi) -= epsilon;
			evalWound(dt, local_dt, ip_Jac, global_parameters, local_parameters,
			  node_rho_0, node_c_0, ip_phic_0, ip_kc_0, ip_a0c_0, ip_kappac_0, ip_phif_0,  ip_kf_0,  ip_a0f_0,  ip_kappaf_0, ip_lamdaP_0, 
			  node_rho,   node_c,  ip_phic,  ip_kc,  ip_a0c,  ip_kappac, ip_phif,  ip_kf,  ip_a0f,  ip_kappaf, ip_lamdaP, node_x_p, 
	  		  Re_x_p, Re_rho_p, Re_c_p);
			evalWound(dt, local_dt, ip_Jac, global_parameters, local_parameters,
			  node_rho_0, node_c_0, ip_phic_0, ip_kc_0, ip_a0c_0, ip_kappac_0, ip_phif_0,  ip_kf_0,  ip_a0f_0,  ip_kappaf_0, ip_lamdaP_0, 
			  node_rho,   node_c,  ip_phic,  ip_kc,  ip_a0c,  ip_kappac, ip_phif,  ip_kf,  ip_a0f,  ip_kappaf, ip_lamdaP, node_x_m, 
	  		  Re_x_m, Re_rho_m, Re_c_m);
			node_x_p[nodei](coordi) -= epsilon;
			node_x_m[nodei](coordi) += epsilon;
			
			// fill in the corresponding entry in the numerical tangent
			for(int nodej=0;nodej<4;nodej++){
				for(int coordj=0;coordj<2;coordj++){
					Ke_x_x_num(nodej*2+coordj,nodei*2+coordi) = (1./(2*epsilon))*(Re_x_p(nodej*2+coordj)-Re_x_m(nodej*2+coordj));
				}
				Ke_rho_x_num(nodej,nodei*2+coordi) = (1./(2*epsilon))*(Re_rho_p(nodej)-Re_rho_m(nodej));
				Ke_c_x_num(nodej,nodei*2+coordi) = (1./(2*epsilon))*(Re_c_p(nodej)-Re_c_m(nodej));
			}
		}
		
		// rho
		node_rho_p[nodei] += epsilon;
		node_rho_m[nodei] -= epsilon;
		evalWound(dt, local_dt, ip_Jac, global_parameters, local_parameters,
			  node_rho_0, node_c_0, ip_phic_0, ip_kc_0, ip_a0c_0, ip_kappac_0, ip_phif_0,  ip_kf_0,  ip_a0f_0,  ip_kappaf_0, ip_lamdaP_0, 
			  node_rho_p,   node_c,  ip_phic,  ip_kc,  ip_a0c,  ip_kappac, ip_phif,  ip_kf,  ip_a0f,  ip_kappaf, ip_lamdaP, node_x, 
	  		  Re_x_p, Re_rho_p, Re_c_p);
		evalWound(dt, local_dt, ip_Jac, global_parameters, local_parameters,
			  node_rho_0, node_c_0, ip_phic_0, ip_kc_0, ip_a0c_0, ip_kappac_0, ip_phif_0,  ip_kf_0,  ip_a0f_0,  ip_kappaf_0, ip_lamdaP_0, 
			  node_rho_m,   node_c,  ip_phic,  ip_kc,  ip_a0c,  ip_kappac, ip_phif,  ip_kf,  ip_a0f,  ip_kappaf, ip_lamdaP, node_x, 
	  		  Re_x_m, Re_rho_m, Re_c_m);
		node_rho_p[nodei] -= epsilon;
		node_rho_m[nodei] += epsilon;	
		for(int nodej=0;nodej<4;nodej++){
			for(int coordj=0;coordj<2;coordj++){
				Ke_x_rho_num(nodej*2+coordj,nodei) = (1./(2*epsilon))*(Re_x_p(nodej*2+coordj)-Re_x_m(nodej*2+coordj));
			}
			Ke_rho_rho_num(nodej,nodei) = (1./(2*epsilon))*(Re_rho_p(nodej)-Re_rho_m(nodej));
			Ke_c_rho_num(nodej,nodei) = (1./(2*epsilon))*(Re_c_p(nodej)-Re_c_m(nodej));
		}
		
		// c
		node_c_p[nodei] += epsilon;
		node_c_m[nodei] -= epsilon;
		evalWound(dt, local_dt, ip_Jac, global_parameters, local_parameters,
			  node_rho_0, node_c_0, ip_phic_0, ip_kc_0, ip_a0c_0, ip_kappac_0, ip_phif_0,  ip_kf_0,  ip_a0f_0,  ip_kappaf_0, ip_lamdaP_0, 
			  node_rho,   node_c_p,  ip_phic,  ip_kc,  ip_a0c,  ip_kappac, ip_phif,  ip_kf,  ip_a0f,  ip_kappaf, ip_lamdaP, node_x, 
	  		  Re_x_p, Re_rho_p, Re_c_p);
		evalWound(dt, local_dt, ip_Jac, global_parameters, local_parameters,
			  node_rho_0, node_c_0, ip_phic_0, ip_kc_0, ip_a0c_0, ip_kappac_0, ip_phif_0,  ip_kf_0,  ip_a0f_0,  ip_kappaf_0, ip_lamdaP_0, 
			  node_rho,   node_c_m,  ip_phic,  ip_kc,  ip_a0c,  ip_kappac, ip_phif,  ip_kf,  ip_a0f,  ip_kappaf, ip_lamdaP, node_x, 
	  		  Re_x_m, Re_rho_m, Re_c_m);
		node_c_p[nodei] -= epsilon;
		node_c_m[nodei] += epsilon;	
		for(int nodej=0;nodej<4;nodej++){
			for(int coordj=0;coordj<2;coordj++){
				Ke_x_c_num(nodej*2+coordj,nodei) = (1./(2*epsilon))*(Re_x_p(nodej*2+coordj)-Re_x_m(nodej*2+coordj));
			}
			Ke_rho_c_num(nodej,nodei) = (1./(2*epsilon))*(Re_rho_p(nodej)-Re_rho_m(nodej));
			Ke_c_c_num(nodej,nodei) = (1./(2*epsilon))*(Re_c_p(nodej)-Re_c_m(nodej));
		}
	}
	
	// print out to see

	// MECHANICS
	//
	std::cout<<"\nRe_x\n"<<Re_x<<"\n";
	//
	std::cout<<"\nKe_x_x\n"<<Ke_x_x;
	std::cout<<"\nKe_x_x_num\n"<<Ke_x_x_num;
	//
	std::cout<<"\n\nKe_x_rho\n"<<Ke_x_rho;
	std::cout<<"\nKe_x_rho_num\n"<<Ke_x_rho_num;
	//
	std::cout<<"\n\nKe_x_c\n"<<Ke_x_c;
	std::cout<<"\nKe_x_c_num\n"<<Ke_x_c_num;

	// RHO
	//
	std::cout<<"\n\n\nRe_rho\n"<<Re_rho<<"\n";
	//
	std::cout<<"\nKe_rho_x\n"<<Ke_rho_x;
	std::cout<<"\nKe_rho_x_num\n"<<Ke_rho_x_num;
	//
	std::cout<<"\n\nKe_rho_rho\n"<<Ke_rho_rho;
	std::cout<<"\nKe_rho_rho_num\n"<<Ke_rho_rho_num;
	//
	std::cout<<"\n\nKe_rho_c\n"<<Ke_x_c;
	std::cout<<"\nKe_rho_c_num\n"<<Ke_x_c_num;
	
	// C
	//
	std::cout<<"\n\n\nRe_c\n"<<Re_c<<"\n";
	//
	std::cout<<"\nKe_c_x\n"<<Ke_c_x;
	std::cout<<"\nKe_c_x_num\n"<<Ke_c_x_num;
	//
	std::cout<<"\n\nKe_c_rho\n"<<Ke_c_rho;
	std::cout<<"\nKe_c_rho_num\n"<<Ke_c_rho_num;
	//
	std::cout<<"\n\nKe_c_c\n"<<Ke_c_c;
	std::cout<<"\nKe_c_c_num\n"<<Ke_c_c_num;

	std::cout<<"\n";
	
}

#ifndef mechanics_h
#define mechanics_h

#include <vector>
#include <map>
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
using namespace Eigen;

Matrix2d evalSS_pas(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, 
	double phic, double mu, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP,const std::vector<double> &parameters);

Matrix3d eval_DDpas(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, 
	double phic, double mu, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP,const std::vector<double> &parameters);

#endif
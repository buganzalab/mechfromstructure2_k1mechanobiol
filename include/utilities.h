/* post processing functions */

#ifndef utilities_h
#define utilities_h

#include "tissue.h" 
#include <vector>
#include <map>
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
using namespace Eigen;


//-------------------------------------------------//
// IO
//-------------------------------------------------//

//----------------------------//
// READ ABAQUS
//----------------------------//
//
// read in an Abaqus input file with mesh and fill in structures
//void readAbaqusInput(const char* filename, tissue &myTissue);

//----------------------------//
// READ COMSOL edited
//----------------------------//
//
// read in an Comsol input file with mesh and fill in structures
void readComsolEditInput(const char* filename,tissue &myTissue);

//----------------------------//
// REAd OWN FILE
//----------------------------//
//
tissue readTissue(const char* filename);

//----------------------------//
// WRITE PARAVIEW
//----------------------------//
//
// write the paraview file with 
//	RHO, C, PHI, THETA_B at the nodes
void writeParaview(tissue &myTissue, const char* filename, double res);
void writeParaview(int FLAG, tissue &myTissue, const char* filename );

//----------------------------//
// WRITE OWN FILE
//----------------------------//
//
void writeTissue(tissue &myTissue, const char* filename,double time, double res);
// Note: what am I passing a time variable?!

//----------------------------//
// WRITE NODE
//----------------------------//
//
// write the node information
// DEFORMED x, RHO, C
void writeNode(tissue &myTissue,const char* filename,int nodei,double time);
// Note: what am I passing a time variable?!

//----------------------------//
// WRITE IP
//----------------------------//
//
// write integration point information
// just ip variables
// PHI A0 KAPPA LAMBDA 
void writeIP(tissue &myTissue,const char* filename,int ipi,double time);

//----------------------------//
// WRITE ELEMENT
//----------------------------//
//
// write an element information to a text file. write the average
// of variables at the center in the following order
//	PHI A0 KAPPA LAMDA_B RHO C
void writeElement(tissue &myTissue,const char* filename,int elemi,double time);


#endif
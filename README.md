# Wound healing simulations with mechanobiological signal mediated by tissue stiffness

This code includes an FEA solver for healing bio-chemo-mechanics in a circular wound that is embedded in a square skin patch.

The work is based on 2017 paper on [CMAME](https://www.sciencedirect.com/science/article/abs/pii/S0045782516302857), and led to the manuscript that is currently archived at [URL].

This code underlies Figure 8 in the manuscript and it is the 4th out of 4 codes that are part of this project.

## Description

Here we consider a more sophisticated link between wound mechanical parameters and its microstructure (fibrin and collagen).
While fibrin affects the tissue mechanics linearly, we now account for the nonlinearities induced by variable collagen crosslinking.

We also consider an alternative approach to represent soft tissue mechanobiology, where the tissue stiffness influences the production of cells and collagens.
Note that to avoid the complexities associated with explicit modeling of the tissue tangent stiffness, we assume the mechanical parameter k1 as a proxy of tissue stiffness.

For a full description of the modeling approach and the obtained results, we refer you to the manuscript currently archived at [URL].

## Getting started

This code is ready for use on Purdue Brown cluster using the job submission file "submit_job.sh", and it will:

1. make the application that solves the wound healing problem

2. solve the healing problem according to the provided input parameters

3. generate vtk files for visualization of the solution, e.g. using ParaView

4. postprocess the data to provide time evolution of multiple variables using the file 'postprocessor.py'

To run it on your machine, make sure to set the correct library path in the makefile and edit the file "submit_job.sh" to your needs.

This code runs with $\Omega^m=0.01$ by default. To generate the other curves shown in Figure 8 in the manuscript ($\Omega^m=0$ -- $\Omega_{\phi}^b=0.8$), you will need to change the multipliers on lines 116 and 173 in /apps/sim_wound_tri_sym.cpp and re-run the job submission script.

## Authors and acknowledgment
This code was developed in 2018-2022 by Marco Pensalfini (then at ETH Zurich, Switzerland) and Adrian Buganza-Tepole (Purdue University, USA).

The work started in the context of Marco's research visit at Purdue, supported by a Doc.Mobility Fellowship from the Swiss National Science Foundation [REF].

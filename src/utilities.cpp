/* implementation of utilities */


#include "utilities.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept> 
#include <math.h> 
#include "wound.h" 
#include "solver.h"
#include <Eigen/Dense> // most of the vector functions I will need inside of an element


using namespace Eigen;

//-------------------------------------------------------------------------------------//
// IO
//-------------------------------------------------------------------------------------//


//---------------------------------------//
// READ COMSOL DERIVED FILE
//---------------------------------------//
//
// read in the text file derived from a COMSOL mesh file and generate the own mesh and fill in 
void readComsolEditInput(const char* filename,tissue &myTissue)
{
	// READ NODES
	std::vector<Vector2d> node_X; node_X.clear();
	std::ifstream myfile(filename);
	std::string line;
	std::string keyword_node = "Nodes";
	double auxX, auxY;
	if (myfile.is_open())
	{
		// read in until you find the keyword Nodes
		while ( getline (myfile,line) )
    	{
      		// check for the keyword
      		std::size_t found = line.find(keyword_node);
			if (found!=std::string::npos)
      		{
      			getline (myfile,line);
				std::stringstream ss0(line);
				ss0>>myTissue.n_node;
      			// found the beginning of the nodes, read the nnumber of nodes
				for(int i =0;i<myTissue.n_node;i++){
					getline (myfile,line);	
					std::stringstream ss1(line);
					ss1>>auxX;
					ss1>>auxY;
					node_X.push_back(Vector2d(auxX,auxY));
				}
      		}
    	}
    }
    myfile.close();
    myTissue.node_X = node_X;
    
	// READ ELEMENTS	
	std::vector<std::vector<int> > LineQuadri; LineQuadri.clear();
	myfile.open(filename);
	std::string keyword_element = "Elements";
	int auxE0,auxE1,auxE2,auxE3;
	if (myfile.is_open())
	{
		// read in until you find the keyword Elements
		while ( getline (myfile,line) )
    	{
      		// check for the keyword
      		std::size_t found = line.find(keyword_element);
			if (found!=std::string::npos)
      		{
      			// found the beginning of the elements, get number of elements and then loop
      			getline (myfile,line);
				std::stringstream ss3(line);
				ss3>>myTissue.n_quadri;
      			for(int i=0;i<myTissue.n_quadri;i++)
      			{
      				getline (myfile,line);
      				std::stringstream ss4(line);
					ss4>>auxE0;
					ss4>>auxE1;
					ss4>>auxE2;
					ss4>>auxE3;
					std::vector<int> elemi = {auxE0,auxE1,auxE3,auxE2};
					//std::cout<<"\n";
					LineQuadri.push_back(elemi);
      			}
      		}
    	}
    }
    myfile.close();
    myTissue.LineQuadri = LineQuadri;	
	
	// in addition to the connectivity and the nodes, some other things
	// myTissue.n_node = node_X.size();
	// myTissue.n_quadri = LineQuadri.size();
	
}

//---------------------------------------//
// READ MY OWN FILE
//---------------------------------------//
tissue readTissue(const char* filename)
{
	// initialize the structure
	tissue myTissue;
	
	std::ifstream myfile(filename);
	std::string line;
	if (myfile.is_open())
	{
		
		// time
		getline (myfile,line);
		std::stringstream ss0(line);
		ss0>>myTissue.time;

		// residual
		getline (myfile,line);

		// time final
		getline (myfile,line);
		std::stringstream ss1(line);
		ss1>>myTissue.time_final;
		
		// time step
		getline (myfile,line);
		std::stringstream ss2(line);
		ss2>>myTissue.time_step;
		
		// tol
		getline (myfile,line);
		std::stringstream ss3(line);
		ss3>>myTissue.tol;
		
		// max iter
		getline (myfile,line);
		std::stringstream ss4(line);
		ss4>>myTissue.max_iter;
		
		// global parameters
		int n_global_parameters;
		getline (myfile,line);
		std::stringstream ss5(line);
		ss5>>n_global_parameters;
		std::vector<double> global_parameters(n_global_parameters,0.);
		getline (myfile,line);
		std::stringstream ss6(line);
		for(int i=0;i<n_global_parameters;i++){
			ss6>>global_parameters[i];
		}
		myTissue.global_parameters = global_parameters;
		
		// local parameters
		int n_local_parameters;
		getline (myfile,line);
		std::stringstream ss7(line);
		ss7>>n_local_parameters;
		std::vector<double> local_parameters(n_local_parameters,0.);
		getline (myfile,line);
		std::stringstream ss8(line);
		for(int i=0;i<n_local_parameters;i++){
			ss8>>local_parameters[i];
		}
		myTissue.local_parameters = local_parameters;
		
		// n_node
		getline (myfile,line);
		std::stringstream ss9(line);
		ss9>>myTissue.n_node;
		
		// n_quadri
		getline (myfile,line);
		std::stringstream ss10(line);
		ss10>>myTissue.n_quadri;
		
		// n_IP
		getline (myfile,line);
		std::stringstream ss11(line);
		ss11>>myTissue.n_IP;
		if(myTissue.n_IP>4*myTissue.n_quadri || myTissue.n_IP<4*myTissue.n_quadri )
		{std::cout<<"number of integration points and elements don't match\n";myTissue.n_IP = 4*myTissue.n_quadri;}
		
		// n_dof
		getline (myfile,line);
		std::stringstream ss12(line);
		ss12>>myTissue.n_dof;
		
		// LineQuadri
		std::vector<int> temp_elem(4,0);
		std::vector<std::vector<int > > LineQuadri(myTissue.n_quadri,temp_elem);
		myTissue.LineQuadri = LineQuadri;
		for(int i=0;i<myTissue.LineQuadri.size();i++){
			getline (myfile,line);
			std::stringstream ss13(line);
			ss13>>myTissue.LineQuadri[i][0]; ss13>>myTissue.LineQuadri[i][1]; ss13>>myTissue.LineQuadri[i][2]; ss13>>myTissue.LineQuadri[i][3];
		}
		
		// node_X 
		std::vector<Vector2d> node_X(myTissue.n_node,Vector2d(0,0));
		myTissue.node_X = node_X;
		for(int i=0;i<myTissue.node_X.size();i++){
			getline (myfile,line);
			std::stringstream ss15(line);
			ss15>>myTissue.node_X[i](0);ss15>>myTissue.node_X[i](1);
		}
		
		// node_rho_0
		std::vector<double> node_rho_0(myTissue.n_node,0.0);
		myTissue.node_rho_0 = node_rho_0;
		for(int i=0;i<myTissue.node_rho_0.size();i++){
			getline (myfile,line);
			std::stringstream ss16(line);
			ss16>>myTissue.node_rho_0[i];
		}
		
		// node_c_0
		std::vector<double> node_c_0(myTissue.n_node,0.0);
		myTissue.node_c_0 = node_c_0;
		for(int i=0;i<myTissue.node_c_0.size();i++){
			getline (myfile,line);
			std::stringstream ss17(line);
			ss17>>myTissue.node_c_0[i];
		}
		
		// ip_phic_0
		std::vector<double> ip_phic_0(myTissue.n_IP,0.0);
		myTissue.ip_phic_0 = ip_phic_0;
		for(int i=0;i<myTissue.ip_phic_0.size();i++){
			getline (myfile,line);
			std::stringstream ss18(line);
			ss18>>myTissue.ip_phic_0[i];
		}
		
		// ip_kc_0
		std::vector<double> ip_kc_0(myTissue.n_IP,0.0);
		myTissue.ip_kc_0 = ip_kc_0;
		for(int i=0;i<myTissue.ip_kc_0.size();i++){
			getline (myfile,line);
			std::stringstream ss18(line);
			ss18>>myTissue.ip_kc_0[i];
		}
		
		// ip_a0c_0
		std::vector<Vector2d> ip_a0c_0(myTissue.n_IP,Vector2d(0,0));
		myTissue.ip_a0c_0 = ip_a0c_0;
		for(int i=0;i<myTissue.ip_a0c_0.size();i++){
			getline (myfile,line);
			std::stringstream ss19(line);
			ss19>>myTissue.ip_a0c_0[i](0);ss19>>myTissue.ip_a0c_0[i](1);
		}
				
		// ip_kappac_0
		std::vector<double> ip_kappac_0(myTissue.n_IP,0.0);
		myTissue.ip_kappac_0 = ip_kappac_0;
		for(int i=0;i<myTissue.ip_kappac_0.size();i++){
			getline (myfile,line);
			std::stringstream ss20(line);
			ss20>>myTissue.ip_kappac_0[i];
		}
		
		// ip_phif_0
		std::vector<double> ip_phif_0(myTissue.n_IP,0.0);
		myTissue.ip_phif_0 = ip_phif_0;
		for(int i=0;i<myTissue.ip_phif_0.size();i++){
			getline (myfile,line);
			std::stringstream ss18(line);
			ss18>>myTissue.ip_phif_0[i];
		}
		
		// ip_kf_0
		std::vector<double> ip_kf_0(myTissue.n_IP,0.0);
		myTissue.ip_kf_0 = ip_kf_0;
		for(int i=0;i<myTissue.ip_kf_0.size();i++){
			getline (myfile,line);
			std::stringstream ss18(line);
			ss18>>myTissue.ip_kf_0[i];
		}
		
		// ip_a0f_0
		std::vector<Vector2d> ip_a0f_0(myTissue.n_IP,Vector2d(0,0));
		myTissue.ip_a0f_0 = ip_a0f_0;
		for(int i=0;i<myTissue.ip_a0f_0.size();i++){
			getline (myfile,line);
			std::stringstream ss19(line);
			ss19>>myTissue.ip_a0f_0[i](0);ss19>>myTissue.ip_a0f_0[i](1);
		}
				
		// ip_kappaf_0
		std::vector<double> ip_kappaf_0(myTissue.n_IP,0.0);
		myTissue.ip_kappaf_0 = ip_kappaf_0;
		for(int i=0;i<myTissue.ip_kappaf_0.size();i++){
			getline (myfile,line);
			std::stringstream ss20(line);
			ss20>>myTissue.ip_kappaf_0[i];
		}
		
		// ip_lamdaP_0
		std::vector<Vector2d> ip_lamdaP_0(myTissue.n_IP,Vector2d(0,0));
		myTissue.ip_lamdaP_0 = ip_lamdaP_0;
		for(int i=0;i<myTissue.ip_lamdaP_0.size();i++){
			getline (myfile,line);
			std::stringstream ss21(line);
			ss21>>myTissue.ip_lamdaP_0[i](0);ss21>>myTissue.ip_lamdaP_0[i](1);
		}
		
		// node_x 
		std::vector<Vector2d> node_x(myTissue.n_node,Vector2d(0,0));
		myTissue.node_x = node_x;
		for(int i=0;i<myTissue.node_x.size();i++){
			getline (myfile,line);
			std::stringstream ss22(line);
			ss22>>myTissue.node_x[i](0);ss22>>myTissue.node_x[i](1);
		}
		
		// node_rho
		myTissue.node_rho = myTissue.node_rho_0;
		
		// node_c
		myTissue.node_c = myTissue.node_c_0;
		
		// ip_phic
		myTissue.ip_phic = myTissue.ip_phic_0;
		
		// ip_kc
		myTissue.ip_kc = myTissue.ip_kc_0;		
		
		// ip_a0c
		myTissue.ip_a0c = myTissue.ip_a0c_0;
		
		// ip_kappac
		myTissue.ip_kappac = myTissue.ip_kappac_0;

		// ip_phif
		myTissue.ip_phif = myTissue.ip_phif_0;
		
		// ip_kf
		myTissue.ip_kf = myTissue.ip_kf_0;		
		
		// ip_a0f
		myTissue.ip_a0f = myTissue.ip_a0f_0;
		
		// ip_kappaf
		myTissue.ip_kappaf = myTissue.ip_kappaf_0;
		
		// ip_lamdaP
		myTissue.ip_lamdaP = myTissue.ip_lamdaP_0;
		
		// eBC_x
		int n_eBC_x;
		int dofx;
		double dofx_value;
		getline (myfile,line);
		std::stringstream ss23(line);
		ss23>>n_eBC_x;
		myTissue.eBC_x.clear();
		for(int i=0;i<n_eBC_x;i++){
			getline (myfile,line);
			std::stringstream ss24(line);
			ss24>>dofx;ss24>>dofx_value;
			myTissue.eBC_x.insert ( std::pair<int,double>(dofx,dofx_value) ); 
		}

		// eBC_rho
		int n_eBC_rho;
		int dofrho;
		double dofrho_value;
		getline (myfile,line);
		std::stringstream ss25(line);
		ss25>>n_eBC_rho;
		myTissue.eBC_rho.clear();
		for(int i=0;i<n_eBC_rho;i++){
			getline (myfile,line);
			std::stringstream ss26(line);
			ss26>>dofrho;ss26>>dofrho_value;
			myTissue.eBC_rho.insert ( std::pair<int,double>(dofrho,dofrho_value) ); 
		}

		// eBC_c
		int n_eBC_c;
		int dofc;
		double dofc_value;
		getline (myfile,line);
		std::stringstream ss27(line);
		ss27>>n_eBC_c;
		myTissue.eBC_c.clear();
		for(int i=0;i<n_eBC_c;i++){
			getline (myfile,line);
			std::stringstream ss28(line);
			ss28>>dofc;ss28>>dofc_value;
			myTissue.eBC_c.insert ( std::pair<int,double>(dofc,dofc_value) ); 
		}

		// nBC_x
		int n_nBC_x;
		double forcex_value;
		getline (myfile,line);
		std::stringstream ss29(line);
		ss29>>n_nBC_x;
		myTissue.nBC_x.clear();
		for(int i=0;i<n_nBC_x;i++){
			getline (myfile,line);
			std::stringstream ss30(line);
			ss30>>dofx;ss30>>forcex_value;
			myTissue.nBC_x.insert ( std::pair<int,double>(dofx,forcex_value) ); 
		}

		// nBC_rho
		int n_nBC_rho;
		double forcerho_value;
		getline (myfile,line);
		std::stringstream ss31(line);
		ss31>>n_nBC_rho;
		myTissue.nBC_rho.clear();
		for(int i=0;i<n_nBC_rho;i++){
			getline (myfile,line);
			std::stringstream ss32(line);
			ss32>>dofrho;ss32>>forcerho_value;
			myTissue.nBC_rho.insert ( std::pair<int,double>(dofrho,forcerho_value) ); 
		}

		// nBC_c
		int n_nBC_c;
		double forcec_value;
		getline (myfile,line);
		std::stringstream ss33(line);
		ss33>>n_nBC_c;
		myTissue.nBC_c.clear();
		for(int i=0;i<n_nBC_c;i++){
			getline (myfile,line);
			std::stringstream ss34(line);
			ss34>>dofc;ss34>>forcec_value;
			myTissue.nBC_c.insert ( std::pair<int,double>(dofc,forcec_value) ); 
		}

		// dof_fwd_map_x
		std::vector<int> dof_fwd_map_x(myTissue.n_node*2,-1);
		myTissue.dof_fwd_map_x = dof_fwd_map_x;
		for(int i=0;i<myTissue.n_node;i++){
			getline (myfile,line);
			std::stringstream ss36(line);
			ss36>>myTissue.dof_fwd_map_x[i*2+0];ss36>>myTissue.dof_fwd_map_x[i*2+1];
		}
	
		// dof_fwd_map_rho
		std::vector<int> dof_fwd_map_rho(myTissue.n_node,-1);
		myTissue.dof_fwd_map_rho = dof_fwd_map_rho;
		for(int i=0;i<myTissue.n_node;i++){
			getline (myfile,line);
			std::stringstream ss38(line);
			ss38>>myTissue.dof_fwd_map_rho[i];
		}
		
		// dof_fwd_map_c
		std::vector<int> dof_fwd_map_c(myTissue.n_node,-1);
		myTissue.dof_fwd_map_c = dof_fwd_map_c;
		for(int i=0;i<myTissue.n_node;i++){
			getline (myfile,line);
			std::stringstream ss40(line);
			ss40>>myTissue.dof_fwd_map_c[i];
		}
	
		// dof_inv_map
		int n_dof_inv_map;
		getline (myfile,line);
		std::stringstream ss41(line);
		ss41>>n_dof_inv_map;
		std::vector<int> temp_inv_dof(2,0);
		std::vector<std::vector<int> > dof_inv_map(n_dof_inv_map,temp_inv_dof);
		myTissue.dof_inv_map = dof_inv_map;
		for(int i=0;i<myTissue.dof_inv_map.size();i++){
			getline (myfile,line);
			std::stringstream ss42(line);
			ss42>>myTissue.dof_inv_map[i][0];ss42>>myTissue.dof_inv_map[i][1];
		}		
	}
	myfile.close();
	evalElemJacobians(myTissue);
	return myTissue;
}


//---------------------------------------//
// WRITE OUT MY OWN FILE
//---------------------------------------//
//
void writeTissue(tissue &myTissue, const char* filename,double time, double res)
{
	std::ofstream savefile(filename);
	if (!savefile) {
		throw std::runtime_error("Unable to open output file.");
	}
	savefile<<time<<"\n";
	savefile<<res<<"\n";
	savefile<<myTissue.time_final<<"\n";
	savefile<<myTissue.time_step<<"\n";
	savefile<<myTissue.tol<<"\n";
	savefile<<myTissue.max_iter<<"\n";	
	savefile<<myTissue.global_parameters.size()<<"\n";
	for(int i=0;i<myTissue.global_parameters.size();i++){
		savefile<<myTissue.global_parameters[i]<<" ";
	}
	savefile<<"\n";
	savefile<<myTissue.local_parameters.size()<<"\n";
	for(int i=0;i<myTissue.local_parameters.size();i++){
		savefile<<myTissue.local_parameters[i]<<" ";
	}
	savefile<<"\n";
	savefile<<myTissue.n_node<<"\n";
	savefile<<myTissue.n_quadri<<"\n";
	savefile<<myTissue.n_IP<<"\n";
	savefile<<myTissue.n_dof<<"\n";
	for(int i=0;i<myTissue.LineQuadri.size();i++){
		savefile<<myTissue.LineQuadri[i][0]<<" "<<myTissue.LineQuadri[i][1]<<" "<<myTissue.LineQuadri[i][2]<<" "<<myTissue.LineQuadri[i][3]<<"\n";
	}
	for(int i=0;i<myTissue.node_X.size();i++){
		savefile<<myTissue.node_X[i](0)<<" "<<myTissue.node_X[i](1)<<"\n";
	}
	for(int i=0;i<myTissue.node_rho_0.size();i++){
		savefile<<myTissue.node_rho_0[i]<<"\n";
	}
	for(int i=0;i<myTissue.node_c_0.size();i++){
		savefile<<myTissue.node_c_0[i]<<"\n";
	}
	
	for(int i=0;i<myTissue.ip_phic_0.size();i++){
		savefile<<myTissue.ip_phic_0[i]<<"\n";
	}
	for(int i=0;i<myTissue.ip_kc_0.size();i++){
		savefile<<myTissue.ip_kc_0[i]<<"\n";
	}
	for(int i=0;i<myTissue.ip_a0c_0.size();i++){
		savefile<<myTissue.ip_a0c_0[i](0)<<" "<<myTissue.ip_a0c_0[i](1)<<"\n";
	}
	for(int i=0;i<myTissue.ip_kappac_0.size();i++){
		savefile<<myTissue.ip_kappac_0[i]<<"\n";
	}	
	
	for(int i=0;i<myTissue.ip_phif_0.size();i++){
		savefile<<myTissue.ip_phif_0[i]<<"\n";
	}
	for(int i=0;i<myTissue.ip_kf_0.size();i++){
		savefile<<myTissue.ip_kf_0[i]<<"\n";
	}
	for(int i=0;i<myTissue.ip_a0f_0.size();i++){
		savefile<<myTissue.ip_a0f_0[i](0)<<" "<<myTissue.ip_a0f_0[i](1)<<"\n";
	}
	for(int i=0;i<myTissue.ip_kappaf_0.size();i++){
		savefile<<myTissue.ip_kappaf_0[i]<<"\n";
	}	
	
	for(int i=0;i<myTissue.ip_lamdaP_0.size();i++){
		savefile<<myTissue.ip_lamdaP_0[i](0)<<" "<<myTissue.ip_lamdaP_0[i](1)<<"\n";
	}
	for(int i=0;i<myTissue.node_x.size();i++){
		savefile<<myTissue.node_x[i](0)<<" "<<myTissue.node_x[i](1)<<"\n";
	}
	std::map<int,double>::iterator it_map_BC;
	savefile<<myTissue.eBC_x.size()<<"\n";
	for(it_map_BC = myTissue.eBC_x.begin(); it_map_BC != myTissue.eBC_x.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}
	savefile<<myTissue.eBC_rho.size()<<"\n";
	for(it_map_BC = myTissue.eBC_rho.begin(); it_map_BC != myTissue.eBC_rho.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}
	savefile<<myTissue.eBC_c.size()<<"\n";
	for(it_map_BC = myTissue.eBC_c.begin(); it_map_BC != myTissue.eBC_c.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}
	savefile<<myTissue.nBC_x.size()<<"\n";
	for(it_map_BC = myTissue.nBC_x.begin(); it_map_BC != myTissue.nBC_x.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}
	savefile<<myTissue.nBC_rho.size()<<"\n";
	for(it_map_BC = myTissue.nBC_rho.begin(); it_map_BC != myTissue.nBC_rho.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}
	savefile<<myTissue.nBC_c.size()<<"\n";
	for(it_map_BC = myTissue.nBC_c.begin(); it_map_BC != myTissue.nBC_c.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}
	
	for(int i=0;i<myTissue.n_node;i++){
		savefile<<myTissue.dof_fwd_map_x[i*2+0]<<" "<<myTissue.dof_fwd_map_x[i*2+1]<<"\n";
	}
	
	for(int i=0;i<myTissue.dof_fwd_map_rho.size();i++){
		savefile<<myTissue.dof_fwd_map_rho[i]<<"\n";
	}

	for(int i=0;i<myTissue.dof_fwd_map_c.size();i++){
		savefile<<myTissue.dof_fwd_map_c[i]<<"\n";
	}
	
	savefile<<myTissue.dof_inv_map.size()<<"\n";
	for(int i=0;i<myTissue.dof_inv_map.size();i++){
		savefile<<myTissue.dof_inv_map[i][0]<<" "<<myTissue.dof_inv_map[i][1]<<"\n";
	}
	savefile.close();
}

//---------------------------------------//
// WRITE OUT A PARAVIEW FILE
//---------------------------------------//
void writeParaview(tissue &myTissue, const char* filename, double res)
{
	std::ofstream savefile(filename);
	if (!savefile) {
		throw std::runtime_error("Unable to open output file.");
	}
	savefile<<"# vtk DataFile Version 2.0\nWound Time: "<<myTissue.time<<", Residual: "<<res<<"\nASCII\nDATASET UNSTRUCTURED_GRID\n";
	savefile<<"POINTS "<<myTissue.node_x.size()<<" double\n";
	for(int i=0;i<myTissue.node_x.size();i++)
	{
		savefile<<myTissue.node_X[i](0)<<" "<<myTissue.node_X[i](1)<<" 0.0\n";
	}
	savefile<<"CELLS "<<myTissue.LineQuadri.size()<<" "<<myTissue.LineQuadri.size()*5<<"\n";
	for(int i=0;i<myTissue.LineQuadri.size();i++)
	{
		savefile<<"4";
		for(int j=0;j<4;j++)
		{
			savefile<<" "<<myTissue.LineQuadri[i][j];
		}
		savefile<<"\n";
	}
	savefile<<"CELL_TYPES "<<myTissue.LineQuadri.size()<<"\n";
	for(int i=0;i<myTissue.LineQuadri.size();i++)
	{
		savefile<<"9\n";
	}
	//std::cout<<"n_node: "<<myTissue.n_node<<"\n";
	//std::cout<<"node_X.size: "<<myTissue.node_X.size()<<"\n";
	//std::cout<<"node_x.size: "<<myTissue.node_x.size()<<"\n";
	//std::cout<<"n_quadri: "<<myTissue.n_quadri<<"\n";
	//std::cout<<"LineQuadri.size: "<<myTissue.LineQuadri.size()<<"\n";
	// SAVE ATTRIBUTES
	// up to four scalars I can plot...
	// first bring back from the integration points to the nodes
	std::vector<double> node_phic(myTissue.n_node,0);
	std::vector<double> node_kc(myTissue.n_node,0);
	std::vector<Vector2d> node_a0c(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_kappac(myTissue.n_node,0);
	//
	std::vector<double> node_phif(myTissue.n_node,0);
	std::vector<double> node_kf(myTissue.n_node,0);
	std::vector<Vector2d> node_a0f(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_kappaf(myTissue.n_node,0);
	//
	std::vector<Vector2d> node_lamdaP(myTissue.n_node,Vector2d(0,0));
	std::vector<int> node_ip_count(myTissue.n_node,0);
	//std::cout<<"saving attributes in paraview file\n";
	for(int elemi=0;elemi<myTissue.n_quadri;elemi++){
		for(int ip=0;ip<4;ip++){
			node_phic[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_phic[elemi*4+ip];
			node_kc[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_kc[elemi*4+ip];
			node_a0c[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_a0c[elemi*4+ip];
			node_kappac[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_kappac[elemi*4+ip];
			//
			node_phif[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_phif[elemi*4+ip];
			node_kf[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_kf[elemi*4+ip];
			node_a0f[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_a0f[elemi*4+ip];
			node_kappaf[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_kappaf[elemi*4+ip];
			//
			node_lamdaP[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_lamdaP[elemi*4+ip];
			node_ip_count[myTissue.LineQuadri[elemi][ip]] += 1;
		}
	}
	for(int nodei=0;nodei<myTissue.n_node;nodei++){
		node_phic[nodei] = node_phic[nodei]/node_ip_count[nodei];
		node_kc[nodei] = node_kc[nodei]/node_ip_count[nodei];
		node_a0c[nodei] = node_a0c[nodei]/node_ip_count[nodei];
		node_kappac[nodei] = node_kappac[nodei]/node_ip_count[nodei];
		//
		node_phif[nodei] = node_phif[nodei]/node_ip_count[nodei];
		node_kf[nodei] = node_kf[nodei]/node_ip_count[nodei];
		node_a0f[nodei] = node_a0f[nodei]/node_ip_count[nodei];
		node_kappaf[nodei] = node_kappaf[nodei]/node_ip_count[nodei];
		//
		node_lamdaP[nodei] = node_lamdaP[nodei]/node_ip_count[nodei];
	}
	//std::cout<<"projected IP to nodes\n";
	// rho, c, phi, theta
	savefile<<"POINT_DATA "<<myTissue.n_node<<"\nSCALARS rho_c float "<<4<<"\nLOOKUP_TABLE default\n";
	for(int i=0;i<myTissue.n_node;i++){
		savefile<<myTissue.node_rho[i]<<" "<<myTissue.node_c[i]<<" "<<node_phic[i]<<" "<<node_lamdaP[i](0)*node_lamdaP[i](1)<<"\n";
	}
	// write out the fiber direction
	savefile<<"VECTORS x float\n";
	for(int i=0;i<myTissue.n_node;i++){
		savefile<<myTissue.node_x[i](0)-myTissue.node_X[i](0)<<" "<<myTissue.node_x[i](1)-myTissue.node_X[i](1)<<" 0\n";
	}
	savefile.close();
}

void writeParaview(int FLAG, tissue &myTissue, const char* filename)
{
	std::ofstream savefile(filename);
	if (!savefile) {
		throw std::runtime_error("Unable to open output file.");
	}
	savefile<<"# vtk DataFile Version 2.0\nWound Time: "<<myTissue.time<<", FLAG: "<<FLAG<<"\nASCII\nDATASET UNSTRUCTURED_GRID\n";
	savefile<<"POINTS "<<myTissue.node_x.size()<<" double\n";
	for(int i=0;i<myTissue.node_x.size();i++)
	{
		savefile<<myTissue.node_X[i](0)<<" "<<myTissue.node_X[i](1)<<" 0.0\n";
	}
	savefile<<"CELLS "<<myTissue.LineQuadri.size()<<" "<<myTissue.LineQuadri.size()*5<<"\n";
	for(int i=0;i<myTissue.LineQuadri.size();i++)
	{
		savefile<<"4";
		for(int j=0;j<4;j++)
		{
			savefile<<" "<<myTissue.LineQuadri[i][j];
		}
		savefile<<"\n";
	}
	savefile<<"CELL_TYPES "<<myTissue.LineQuadri.size()<<"\n";
	for(int i=0;i<myTissue.LineQuadri.size();i++)
	{
		savefile<<"9\n";
	}
	//std::cout<<"n_node: "<<myTissue.n_node<<"\n";
	//std::cout<<"node_X.size: "<<myTissue.node_X.size()<<"\n";
	//std::cout<<"node_x.size: "<<myTissue.node_x.size()<<"\n";
	//std::cout<<"n_quadri: "<<myTissue.n_quadri<<"\n";
	//std::cout<<"LineQuadri.size: "<<myTissue.LineQuadri.size()<<"\n";
	// SAVE ATTRIBUTES
	// up to four scalars I can plot...
	// first bring back from the integration points to the nodes
	std::vector<double> node_phic(myTissue.n_node,0);
	std::vector<double> node_kc(myTissue.n_node,0);
	std::vector<Vector2d> node_a0c(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_kappac(myTissue.n_node,0);
	//
	std::vector<double> node_phif(myTissue.n_node,0);
	std::vector<double> node_kf(myTissue.n_node,0);
	std::vector<Vector2d> node_a0f(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_kappaf(myTissue.n_node,0);
	//
	std::vector<Vector2d> node_lamdaP(myTissue.n_node,Vector2d(0,0));
	std::vector<int> node_ip_count(myTissue.n_node,0);
	//std::cout<<"saving attributes in paraview file\n";
	for(int elemi=0;elemi<myTissue.n_quadri;elemi++){
		for(int ip=0;ip<4;ip++){
			node_phic[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_phic[elemi*4+ip];
			node_kc[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_kc[elemi*4+ip];
			node_a0c[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_a0c[elemi*4+ip];
			node_kappac[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_kappac[elemi*4+ip];
			//
			node_phif[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_phif[elemi*4+ip];
			node_kf[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_kf[elemi*4+ip];
			node_a0f[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_a0f[elemi*4+ip];
			node_kappaf[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_kappaf[elemi*4+ip];
			//
			node_lamdaP[myTissue.LineQuadri[elemi][ip]]+=myTissue.ip_lamdaP[elemi*4+ip];
			node_ip_count[myTissue.LineQuadri[elemi][ip]] += 1;
			//
			// inside of the quadrature point you can calculate the strain
		}
	}
	for(int nodei=0;nodei<myTissue.n_node;nodei++){
		node_phic[nodei] = node_phic[nodei]/node_ip_count[nodei];
		node_kc[nodei] = node_kc[nodei]/node_ip_count[nodei];
		node_a0c[nodei] = node_a0c[nodei]/node_ip_count[nodei];
		node_kappac[nodei] = node_kappac[nodei]/node_ip_count[nodei];
		//
		node_phif[nodei] = node_phif[nodei]/node_ip_count[nodei];
		node_kf[nodei] = node_kf[nodei]/node_ip_count[nodei];
		node_a0f[nodei] = node_a0f[nodei]/node_ip_count[nodei];
		node_kappaf[nodei] = node_kappaf[nodei]/node_ip_count[nodei];
		//
		node_lamdaP[nodei] = node_lamdaP[nodei]/node_ip_count[nodei];
	}
	//std::cout<<"projected IP to nodes\n";
	// rho, c, phi, theta
	if(FLAG==0){
		savefile<<"POINT_DATA "<<myTissue.n_node<<"\nSCALARS rho_c float "<<4<<"\nLOOKUP_TABLE default\n";
		for(int i=0;i<myTissue.n_node;i++){
			savefile<<myTissue.node_rho[i]<<" "<<myTissue.node_c[i]<<" "<<node_phic[i]<<" "<<node_lamdaP[i](0)*node_lamdaP[i](1)<<"\n";
		}
	}
	// write out the fiber direction
	savefile<<"VECTORS x float\n";
	for(int i=0;i<myTissue.n_node;i++){
		savefile<<myTissue.node_x[i](0)-myTissue.node_X[i](0)<<" "<<myTissue.node_x[i](1)-myTissue.node_X[i](1)<<" 0\n";
	}
	savefile.close();
}


//---------------------------------------//
// write NODE data to a file
//---------------------------------------//
void writeNode(tissue &myTissue,const char* filename,int nodei,double time)
{
	// write node i to a file
	std::ofstream savefile;
	savefile.open(filename, std::ios_base::app);
	savefile<< time<<","<<myTissue.node_x[nodei](0)<<","<<myTissue.node_x[nodei](1)<<","<<myTissue.node_rho[nodei]<<","<<myTissue.node_c[nodei]<<"\n";
	savefile.close();
}

//---------------------------------------//
// write Integration Point data to a file
//---------------------------------------//
void writeIP(tissue &myTissue,const char* filename,int ipi,double time)
{
	// write integration point i to a file
	std::ofstream savefile;
	savefile.open(filename, std::ios_base::app);
	savefile<< time<<","<<myTissue.ip_phic[ipi]<<","<<myTissue.ip_kc[ipi]<<myTissue.ip_a0c[ipi](0)<<","<<myTissue.ip_a0c[ipi](1)<<","<<myTissue.ip_kappac[ipi]<<","<<myTissue.ip_phif[ipi]<<","<<myTissue.ip_kf[ipi]<<myTissue.ip_a0f[ipi](0)<<","<<myTissue.ip_a0f[ipi](1)<<","<<myTissue.ip_kappaf[ipi]<<","<<myTissue.ip_lamdaP[ipi](0)<<","<<myTissue.ip_lamdaP[ipi](1)<<"\n";
	savefile.close();
}

//---------------------------------------//
// write Element data to a file
//---------------------------------------//
void writeElement(tissue &myTissue,const char* filename,int elemi,double time)
{
	// write element i to a file
	std::ofstream savefile;
	savefile.open(filename, std::ios_base::app);
	// average the nodes and the integration points of this element
	std::vector<int> element = myTissue.LineQuadri[elemi];
	double rho=0;
	double c = 0;
	Vector2d x;x.setZero();
	double phic = 0;
	double phif = 0;
	double kc = 0;
	double kf = 0;
	Vector2d a0c;a0c.setZero();
	Vector2d a0f;a0f.setZero();
	double kappac = 0;
	double kappaf = 0;
	Vector2d lamdaP;lamdaP.setZero();
	for(int i=0;i<4;i++){
		x += myTissue.node_x[element[i]];
		rho += myTissue.node_rho[element[i]];
		c += myTissue.node_c[element[i]];
		phic += myTissue.ip_phic[elemi*4+i];
		kc += myTissue.ip_kc[elemi*4+i];
		a0c += myTissue.ip_a0c[elemi*4+i];
		kappac += myTissue.ip_kappac[elemi*4+i];
		phif += myTissue.ip_phif[elemi*4+i];
		kf += myTissue.ip_kf[elemi*4+i];
		a0f += myTissue.ip_a0f[elemi*4+i];
		kappaf += myTissue.ip_kappaf[elemi*4+i];
		lamdaP += myTissue.ip_lamdaP[elemi*4+i];
	}
	x = x/4.; rho = rho/4.; c = c/4.;
	phic = phic/4.; kc = kc/4.; a0c = a0c/4.; kappac = kappac/4.; 
	phif = phif/4.; kf = kf/4.; a0f = a0f/4.; kappaf = kappaf/4.; 
	lamdaP = lamdaP/4.;
	savefile<<time<<","<<phic<<","<<kc<<","<<a0c(0)<<","<<a0c(1)<<","<<kappac<<","<<phif<<","<<kf<<","<<a0f(0)<<","<<a0f(1)<<","<<kappaf<<","<<lamdaP(0)<<","<<lamdaP(1)<<","<<rho<<","<<c<<"\n";
}
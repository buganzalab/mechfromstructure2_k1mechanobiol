/*
	Pre and Post processing functions
	Solver functions
	Struct and classes for the problem definition
*/


#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept> 
#include <math.h> 
#include "wound.h" 
#include "solver.h"
#include "utilities.h"
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
#include <Eigen/Sparse> // functions for solution of linear systems
#include <Eigen/OrderingMethods>
typedef Eigen::SparseMatrix<double> SpMat; // declares a column-major sparse matrix type of double
typedef Eigen::Triplet<double> T;

using namespace Eigen;

//-------------------------------------------------------------------------------------//
// PRE PROCESSING
//-------------------------------------------------------------------------------------//


//---------------------------------------//
// FILL IN DOF MAPS
//---------------------------------------//
// NOTE: the structure has an empty dof map, but it already has the eBC, the essential
// boundary condition maps
void fillDOFmap(tissue &myTissue)
{
	// some mesh values
	int n_node = myTissue.node_X.size();
	int n_elem  = myTissue.LineQuadri.size();
	
	// initialize the dof count and the maps
	int dof_count = 0;
	int dof_count_xx = 0;
	// displacements
	std::vector< int > dof_fwd_map_x(n_node*2,-1);
	std::vector< int > dof_fwd_map_xx(n_node*2,-1);
	// concentrations
	std::vector< int > dof_fwd_map_rho(n_node,-1);
	std::vector< int > dof_fwd_map_c(n_node,-1);
		
	// all dof inverse map
	std::vector< std::vector<int> > dof_inv_map;
	std::vector<int> dof_inv_map_xx;
	
	// loop over the node set
	for(int i=0;i<n_node;i++)
	{
		// check if node has essential boundary conditions for displacements
		for(int j=0; j<2; j++)
		{
			if(myTissue.eBC_x.find(i*2+j)==myTissue.eBC_x.end())
			{
				// no eBC_x, means this is a DOF
				// fill in forward map
				dof_fwd_map_x[i*2+j] = dof_count;
				dof_fwd_map_xx[i*2+j] = dof_count_xx;
				// fill in inverse map
				std::vector<int> dofinvx = {0,i*2+j};
				dof_inv_map.push_back(dofinvx);
				dof_inv_map_xx.push_back(i*2+j);
				dof_count+=1;
				dof_count_xx+=1;
			}else{
				// this node is in fact in the eBC
				myTissue.node_x[i](j) = myTissue.eBC_x.find(i*2+j)->second;
			}
		}		
		if(myTissue.eBC_rho.find(i)==myTissue.eBC_rho.end())
		{
			dof_fwd_map_rho[i] = dof_count;
			std::vector<int> dofinvrho = {1,i};
			dof_inv_map.push_back(dofinvrho);
			dof_count+=1;
		}else{
			// this node is in fact in the eBC, 
			myTissue.node_rho[i] = myTissue.eBC_rho.find(i)->second;
		}
		if(myTissue.eBC_c.find(i)==myTissue.eBC_c.end())
		{
			dof_fwd_map_c[i] = dof_count;
			std::vector<int> dofinvc = {2,i};
			dof_inv_map.push_back(dofinvc);
			dof_count+=1;
		}else{
			// this node is in fact in the eBC, 
			myTissue.node_c[i] = myTissue.eBC_c.find(i)->second;
		}
	}
	myTissue.dof_fwd_map_x = dof_fwd_map_x;
	myTissue.dof_fwd_map_rho = dof_fwd_map_rho;
	myTissue.dof_fwd_map_c = dof_fwd_map_c;
	myTissue.dof_inv_map = dof_inv_map;
	myTissue.n_dof = dof_count;
	myTissue.n_dof_xx = dof_count_xx;
	myTissue.dof_fwd_map_xx = dof_fwd_map_xx;
	myTissue.dof_inv_map_xx = dof_inv_map_xx;
}


//---------------------------------------//
// EVAL JACOBIANS
//---------------------------------------//

// NOTE: assume the mesh and boundary conditions have already been read. The following
// function stores the internal element variables, namely the Jacobians, maybe some other
// thing, but the Jacobians is the primary thing
//
// EVAL JACOBIANS
void evalElemJacobians(tissue &myTissue)
{
	// clear the vectors
	std::vector<std::vector<Matrix2d> > elem_jac_IP;
	// loop over the elements
	int n_elem = myTissue.LineQuadri.size();
	//std::cout<<"evaluating element jacobians, over "<<n_elem<<" elements\n";
	for(int ei=0;ei<n_elem;ei++)
	{
		// this element connectivity
		std::vector<int> elem = myTissue.LineQuadri[ei];
		// nodal positions for this element
		std::vector<Vector2d> node_X_ni;
		for(int ni=0;ni<4;ni++)
		{
			node_X_ni.push_back(myTissue.node_X[elem[ni]]);
		}
		// compute the vector of jacobians
		std::vector<Matrix2d> jac_IPi = evalJacobian(node_X_ni);
		elem_jac_IP.push_back(jac_IPi);
	}
	// assign to the structure
	myTissue.elem_jac_IP = elem_jac_IP;
}


//-------------------------------------------------------------------------------------//
// SOLVER
//-------------------------------------------------------------------------------------//


//---------------------------------------//
// SPARSE SOLVER
//---------------------------------------//
//
// NOTE: at this point the struct is ready with all that is needed, including boundary and
// initial conditions. Also, the internal constants, namely the Jacobians, have been
// calculated and stored. Time to solve the global system
void sparseWoundSolver(tissue &myTissue, std::string filename, int save_freq,const std::vector<int> &save_node,const std::vector<int> &save_ip)
{
	int n_dof = myTissue.n_dof;
	std::cout<<"I will solve a small system of "<<n_dof<<" dof\n";
    VectorXd RR(n_dof);
	VectorXd SOL(n_dof);SOL.setZero();
    //SpMat KK(n_dof,n_dof); // sparse to solve with BiCG
    SparseMatrix<double, ColMajor> KK2(n_dof,n_dof);
	std::vector<T> KK_triplets;KK_triplets.clear();
	SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> >   solver2;
	//std::cout<<"start parameters\n";
	// PARAMETERS FOR THE SIMULATION
	double time_final = myTissue.time_final;
	double time_step  = myTissue.time_step;
	double local_time_step = myTissue.local_time_step;
	double time = myTissue.time;
	int max_time_step_doublings = myTissue.max_time_step_doublings;
	double min_time_step = time_step/pow(2,max_time_step_doublings);
	
	// Save an original configuration file
	std::stringstream ss;
	ss << "REF";
	std::string filename_step = filename + ss.str()+".vtk";
	//std::cout<<"write paraview\n";
	writeParaview(myTissue,filename_step.c_str(),0);
	//std::cout<<"declare variables for the solver\n";
	
	//------------------------------------//
	// DECLARE VARIABLES YOU'LL USE LATER
	// nodal positions for the element
	std::vector<Vector2d> node_x_ni;
	//			
	// concentration and cells for previous and current time
	std::vector<double> node_rho_0_ni;
	std::vector<double> node_c_0_ni;
	std::vector<double> node_rho_ni;
	std::vector<double> node_c_ni;
	//			
	// values of the structural variables at the IP
	//
	std::vector<double> ip_phic_0_pi;
	std::vector<double> ip_kc_0_pi;
	std::vector<Vector2d> ip_a0c_0_pi;
	std::vector<double> ip_kappac_0_pi;
	//
	std::vector<double> ip_phif_0_pi;
	std::vector<double> ip_kf_0_pi;
	std::vector<Vector2d> ip_a0f_0_pi;
	std::vector<double> ip_kappaf_0_pi;
	//	
	std::vector<Vector2d> ip_lamdaP_0_pi;
	//
	std::vector<double> ip_phic_pi;
	std::vector<double> ip_kc_pi;
	std::vector<Vector2d> ip_a0c_pi;
	std::vector<double> ip_kappac_pi;
	//
	std::vector<double> ip_phif_pi;
	std::vector<double> ip_kf_pi;
	std::vector<Vector2d> ip_a0f_pi;
	std::vector<double> ip_kappaf_pi;	
	//
	std::vector<Vector2d> ip_lamdaP_pi;
	//
    // pieces of the Residuals
    VectorXd Re_x(8);
    VectorXd Re_rho(4); 
    VectorXd Re_c(4); 
	//
    // pieces of the Tangents
	MatrixXd Ke_x_x(8,8);
	MatrixXd Ke_x_rho(8,4);
	MatrixXd Ke_x_c(8,4);
	MatrixXd Ke_rho_x(4,8);
	MatrixXd Ke_rho_rho(4,4);
	MatrixXd Ke_rho_c(4,4);
	MatrixXd Ke_c_x(4,8);
	MatrixXd Ke_c_rho(4,4);
	MatrixXd Ke_c_c(4,4);
	//
	//------------------------------------//
	

	// LOOP OVER TIME
	std::cout<<"start loop over time\n";
	int max_steps = 4e6;
	int time_step_doublings = 0;
	double flag_res = 1;
	for(int step=0;step<max_steps;step++)
	{
		// GLOBAL NEWTON-RAPHSON ITERATION
		int iter = 0;
		double residuum  = 1.;
		double residuum0 = 1.;
		// create a copy of the nodal positions
		std::vector<Vector2d> node_x_copy = myTissue.node_x;	
		//std::cout<<"tissue tolerance: "<<myTissue.tol<<"\n";
		//std::cout<<"max iterations: "<<myTissue.max_iter<<"\n";
		while(residuum>myTissue.tol && iter<myTissue.max_iter)
		{
            // reset the solvers
            KK2.setZero();
            RR.setZero();
            KK_triplets.clear();
            SOL.setZero();
            
            // START LOOP OVER ELEMENTS
            int n_elem = myTissue.LineQuadri.size();
            for(int ei=0;ei<n_elem;ei++)
            {
            	// element stuff
            	
            	// connectivity of the linear elements
            	std::vector<int> elem_ei = myTissue.LineQuadri[ei];
            	//std::cout<<"element "<<ei<<": "<<elem_ei[0]<<","<<elem_ei[1]<<","<<elem_ei[2]<<","<<elem_ei[3]<<"\n";
            	
				// nodal positions for this element
				node_x_ni.clear();
				
				// concentration and cells for previous and current time
				node_rho_0_ni.clear();
				node_c_0_ni.clear();
				node_rho_ni.clear();
				node_c_ni.clear();
				
				// values of the structural variables at the IP
				ip_phic_0_pi.clear();
				ip_kc_0_pi.clear();
				ip_a0c_0_pi.clear();
				ip_kappac_0_pi.clear();
				//
				ip_phif_0_pi.clear();
				ip_kf_0_pi.clear();
				ip_a0f_0_pi.clear();
				ip_kappaf_0_pi.clear();
				//
				ip_lamdaP_0_pi.clear();
				//
				ip_phic_pi.clear();
				ip_kc_pi.clear();
				ip_a0c_pi.clear();
				ip_kappac_pi.clear();
				//
				ip_phif_pi.clear();
				ip_kf_pi.clear();
				ip_a0f_pi.clear();
				ip_kappaf_pi.clear();
				//
				ip_lamdaP_pi.clear();
	
				for(int ni=0;ni<4;ni++){
					// deformed positions
					node_x_ni.push_back(myTissue.node_x[elem_ei[ni]]);
					//std::cout<<"pushing node "<<elem_ei[ni]<<": "<<node_x_ni[ni](0)<<","<<node_x_ni[ni](1)<<"\n";
					
					// cells and chemical
					node_rho_0_ni.push_back(myTissue.node_rho_0[elem_ei[ni]]);
					node_c_0_ni.push_back(myTissue.node_c_0[elem_ei[ni]]);
					node_rho_ni.push_back(myTissue.node_rho[elem_ei[ni]]);
					node_c_ni.push_back(myTissue.node_c[elem_ei[ni]]);
					
					// structural variables
					// conveniently inside this loop because n_ip = n_node
					ip_phic_0_pi.push_back(myTissue.ip_phic_0[ei*4+ni]);
					ip_phic_pi.push_back(myTissue.ip_phic[ei*4+ni]);
					//
					ip_kc_0_pi.push_back(myTissue.ip_kc_0[ei*4+ni]);
					ip_kc_pi.push_back(myTissue.ip_kc[ei*4+ni]);
					//
					ip_phif_0_pi.push_back(myTissue.ip_phif_0[ei*4+ni]);
					ip_phif_pi.push_back(myTissue.ip_phif[ei*4+ni]);
					//
					ip_kf_0_pi.push_back(myTissue.ip_kf_0[ei*4+ni]);
					ip_kf_pi.push_back(myTissue.ip_kf[ei*4+ni]);
					//
					ip_a0c_0_pi.push_back(myTissue.ip_a0c_0[ei*4+ni]);
					ip_a0c_pi.push_back(myTissue.ip_a0c[ei*4+ni]);
					//
					ip_a0f_0_pi.push_back(myTissue.ip_a0f_0[ei*4+ni]);
					ip_a0f_pi.push_back(myTissue.ip_a0f[ei*4+ni]);
					//
					ip_kappac_0_pi.push_back(myTissue.ip_kappac_0[ei*4+ni]);
					ip_kappac_pi.push_back(myTissue.ip_kappac[ei*4+ni]);
					//
					ip_kappaf_0_pi.push_back(myTissue.ip_kappaf_0[ei*4+ni]);
					ip_kappaf_pi.push_back(myTissue.ip_kappaf[ei*4+ni]);
					//
					ip_lamdaP_0_pi.push_back(myTissue.ip_lamdaP_0[ei*4+ni]);
					ip_lamdaP_pi.push_back(myTissue.ip_lamdaP[ei*4+ni]);
				}
				
            	// and calculate the element Re and Ke
            	// pieces of the Residuals
            	Re_x.setZero();
            	Re_rho.setZero(); 
            	Re_c.setZero(); 

            	// pieces of the Tangents
				Ke_x_x.setZero();
				Ke_x_rho.setZero();
				Ke_x_c.setZero();
				Ke_rho_x.setZero();
				Ke_rho_rho.setZero();
				Ke_rho_c.setZero();
				Ke_c_x.setZero();
				Ke_c_rho.setZero();
				Ke_c_c.setZero();

            	// subroutines to evaluate the element
            	//
            	//std::cout<<"wound at element "<<ei<<"\n";
            	evalWound(
					time_step,local_time_step,
					myTissue.elem_jac_IP[ei],
					myTissue.global_parameters,myTissue.local_parameters,
					node_rho_0_ni,node_c_0_ni,
					ip_phic_0_pi,ip_kc_0_pi,ip_a0c_0_pi,ip_kappac_0_pi,
					ip_phif_0_pi,ip_kf_0_pi,ip_a0f_0_pi,ip_kappaf_0_pi,
					ip_lamdaP_0_pi,
					node_rho_ni, node_c_ni,
					ip_phic_pi,ip_kc_pi,ip_a0c_pi,ip_kappac_pi,
					ip_phif_pi,ip_kf_pi,ip_a0f_pi,ip_kappaf_pi,
					ip_lamdaP_pi,
					node_x_ni,
					Re_x, Ke_x_x, Ke_x_rho, Ke_x_c,
					Re_rho, Ke_rho_x, Ke_rho_rho, Ke_rho_c,
					Re_c, Ke_c_x, Ke_c_rho, Ke_c_c);
            	//std::cout<<"Re_X\n";
            	//std::cout<<Re_x;
				//std::cout<<"Ke_x_x\n"<<Ke_x_x<<"\n";
				//std::cout<<"Ke_x_rho\n"<<Ke_x_rho<<"\n";
				//std::cout<<"Ke_x_c\n"<<Ke_x_c<<"\n";
				//std::cout<<"Ke_rho_x\n"<<Ke_rho_x<<"\n";
				//std::cout<<"Ke_rho_rho\n"<<Ke_rho_rho<<"\n";
				//std::cout<<"Ke_rho_c\n"<<Ke_rho_c<<"\n";
				//std::cout<<"Ke_c_x\n"<<Ke_c_x<<"\n";
				//std::cout<<"Ke_c_rho\n"<<Ke_c_rho<<"\n";
				//std::cout<<"Ke_c_c\n"<<Ke_c_c<<"\n";
				// store the new IP values
            	for(int ipi=0;ipi<4;ipi++){
            		myTissue.ip_phic[ei*4+ipi] = ip_phic_pi[ipi];
            		myTissue.ip_kc[ei*4+ipi] = ip_kc_pi[ipi];
            		myTissue.ip_a0c[ei*4+ipi] = ip_a0c_pi[ipi];
            		myTissue.ip_kappac[ei*4+ipi] = ip_kappac_pi[ipi];
            		//
            		myTissue.ip_phif[ei*4+ipi] = ip_phif_pi[ipi];
            		myTissue.ip_kf[ei*4+ipi] = ip_kf_pi[ipi];
            		myTissue.ip_a0f[ei*4+ipi] = ip_a0f_pi[ipi];
            		myTissue.ip_kappaf[ei*4+ipi] = ip_kappaf_pi[ipi];
            		//
            		myTissue.ip_lamdaP[ei*4+ipi] = ip_lamdaP_pi[ipi];
            	}
            	//std::cout<<"done with  wound\n";
            	// assemble into KK triplets array and RR

            	// LOOP OVER NODES
				for(int nodei=0;nodei<4;nodei++){
					// ASSEMBLE DISPLACEMENT RESIDUAL AND TANGENTS
					for(int coordi=0;coordi<2;coordi++){
						if(myTissue.dof_fwd_map_x[elem_ei[nodei]*2+coordi]>-1){
							// residual
							RR(myTissue.dof_fwd_map_x[elem_ei[nodei]*2+coordi]) += Re_x(nodei*2+coordi);
							// loop over displacement dof for the tangent
							for(int nodej=0;nodej<4;nodej++){
								for(int coordj=0;coordj<2;coordj++){
									if(myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj]>-1){
										T K_x_x_nici_njcj = {myTissue.dof_fwd_map_x[elem_ei[nodei]*2+coordi],myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj],Ke_x_x(nodei*2+coordi,nodej*2+coordj)};
										KK_triplets.push_back(K_x_x_nici_njcj);
									}
								}
								// rho tangent
								if(myTissue.dof_fwd_map_rho[elem_ei[nodej]]>-1){
									T K_x_rho_nici_nj = {myTissue.dof_fwd_map_x[elem_ei[nodei]*2+coordi],myTissue.dof_fwd_map_rho[elem_ei[nodej]],Ke_x_rho(nodei*2+coordi,nodej)};
									KK_triplets.push_back(K_x_rho_nici_nj);
								}
								// C tangent
								if(myTissue.dof_fwd_map_c[elem_ei[nodej]]>-1){
									T K_x_c_nici_nj = {myTissue.dof_fwd_map_x[elem_ei[nodei]*2+coordi],myTissue.dof_fwd_map_c[elem_ei[nodej]],Ke_x_c(nodei*2+coordi,nodej)};
									KK_triplets.push_back(K_x_c_nici_nj);
								}
							}
						}
					}
					// ASSEMBLE RHO
					if(myTissue.dof_fwd_map_rho[elem_ei[nodei]]>-1){
						RR(myTissue.dof_fwd_map_rho[elem_ei[nodei]]) += Re_rho(nodei);
						// tangent of the rho
						for(int nodej=0;nodej<4;nodej++){
							for(int coordj=0;coordj<2;coordj++){
								if(myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj]>-1){
									T K_rho_x_ni_njcj = {myTissue.dof_fwd_map_rho[elem_ei[nodei]],myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj],Ke_rho_x(nodei,nodej*2+coordj)};
									KK_triplets.push_back(K_rho_x_ni_njcj);
								}
							}
							if(myTissue.dof_fwd_map_rho[elem_ei[nodej]]>-1){
								T K_rho_rho_ni_nj = {myTissue.dof_fwd_map_rho[elem_ei[nodei]],myTissue.dof_fwd_map_rho[elem_ei[nodej]],Ke_rho_rho(nodei,nodej)};
								KK_triplets.push_back(K_rho_rho_ni_nj);
							}
							if(myTissue.dof_fwd_map_c[elem_ei[nodej]]>-1){
								T K_rho_c_ni_nj = {myTissue.dof_fwd_map_rho[elem_ei[nodei]],myTissue.dof_fwd_map_c[elem_ei[nodej]],Ke_rho_c(nodei,nodej)};
								KK_triplets.push_back(K_rho_c_ni_nj);
							}
						}
					}
					// ASSEMBLE C
					if(myTissue.dof_fwd_map_c[elem_ei[nodei]]>-1){
						RR(myTissue.dof_fwd_map_c[elem_ei[nodei]]) += Re_c(nodei);
						// tangent of the C
						for(int nodej=0;nodej<4;nodej++){
							for(int coordj=0;coordj<2;coordj++){
								if(myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj]>-1){
									T K_c_x_ni_njcj = {myTissue.dof_fwd_map_c[elem_ei[nodei]],myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj],Ke_c_x(nodei,nodej*2+coordj)};
									KK_triplets.push_back(K_c_x_ni_njcj);
								}
							}
							if(myTissue.dof_fwd_map_rho[elem_ei[nodej]]>-1){
								T K_c_rho_ni_nj = {myTissue.dof_fwd_map_c[elem_ei[nodei]],myTissue.dof_fwd_map_rho[elem_ei[nodej]],Ke_c_rho(nodei,nodej)};
								KK_triplets.push_back(K_c_rho_ni_nj);
							}
							if(myTissue.dof_fwd_map_c[elem_ei[nodej]]>-1){
								T K_c_c_ni_nj = {myTissue.dof_fwd_map_c[elem_ei[nodei]],myTissue.dof_fwd_map_c[elem_ei[nodej]],Ke_c_c(nodei,nodej)};
								KK_triplets.push_back(K_c_c_ni_nj);
							}
						}
					}
				}
				// FINISH LOOP OVER NODES (for assembly)
			}
			// FINISH LOOP OVER ELEMENTS
			
			
			// residual norm
			double normRR = sqrt(RR.dot(RR));
			//std::cout<<"RR\n"<<RR<<"\n";
			//break;
			if(iter==0){
				//std::cout<<"first residual\n"<<RR<<"\nRe_rho\n"<<Re_rho<<"\nRe_c\n"<<Re_c<<"\n";
				residuum0 = normRR;
				if(residuum0<myTissue.tol){std::cout<<"no need to solve?: "<<residuum0<<"\n";break;}
				//std::cout<<"first tangents\nKe_x_x\n"<<Ke_x_x<<"\nKe_x_rho\n"<<Ke_x_rho<<"\nKe_x_c\n"<<Ke_x_c<<"\n";
				//std::cout<<"first tangents\nKe_rho_x\n"<<Ke_rho_x<<"\nKe_rho_rho\n"<<Ke_rho_rho<<"\nKe_rho_c\n"<<Ke_rho_c<<"\n";
				//std::cout<<"first tangents\nKe_c_x\n"<<Ke_c_x<<"\nKe_c_rho\n"<<Ke_c_rho<<"\nKe_c_c\n"<<Ke_c_c<<"\n";
			}
			else{residuum = normRR/(1+residuum0);}
			flag_res = residuum;
			// SOLVE: one approach
			//std::cout<<"solve\n";
			//KK.setFromTriplets(KK_triplets.begin(), KK_triplets.end());
			KK2.setFromTriplets(KK_triplets.begin(), KK_triplets.end());
			KK2.makeCompressed();
			//std::cout<<"KK2\n"<<KK2<<"\n";
			//solver2.analyzePattern(KK2); 
			// Compute the numerical factorization 
			//solver2.factorize(KK2); 
			solver2.compute(KK2);
			//Use the factors to solve the linear system 
			SOL = solver2.solve(-1.*RR); 
			
			// SOLVE: alternate
			//BiCGSTAB<SparseMatrix<double> > solver;
			//solver.compute(KK2);
			//VectorXd SOL = solver.solve(-RR);
			//std::cout << "#iterations:     " << solver.iterations() << std::endl;
  			//std::cout << "estimated error: " << solver.error()      << std::endl;
			//std::cout<<SOL2<<"\n";
			
			// update the solution
			double normSOL = sqrt(SOL.dot(SOL));
			for(int dofi=0;dofi<n_dof;dofi++)
			{
				std::vector<int> dof_inv_i = myTissue.dof_inv_map[dofi];
				if(dof_inv_i[0]==0){
					// displacement dof
					int nodei  = dof_inv_i[1]/2;
					int coordi = dof_inv_i[1]%2;
					myTissue.node_x[nodei](coordi)+=SOL(dofi);
				}else if(dof_inv_i[0]==1){
					// rho dof
					myTissue.node_rho[dof_inv_i[1]] += SOL(dofi);
				}else if(dof_inv_i[0]==2){
					// C dof
					myTissue.node_c[dof_inv_i[1]] += SOL(dofi);
				}
			}
			iter += 1;
			if(iter == myTissue.max_iter){
				if(time_step_doublings<max_time_step_doublings){
					std::cout<<"\n\n\nNOOOOOOOOO... restart with smaller time step\n\n\n";
					iter = 0;
					residuum0 = 1.0;
					residuum = 1.0;
					time_step = 0.5*time_step;
					time_step_doublings += 1;	
					myTissue.node_x = node_x_copy;
				}else{
					std::cout<<"\n\n\nCRAAAAAAAAP... \n\n\n";
				}
			}
			std::cout<<"End of iteration : "<<iter<<",\nResidual before increment: "<<normRR<<"\nIncrement norm: "<<normSOL<<"\n\n";
		}
		// FINISH WHILE LOOP OF NEWTON INCREMENTS
		// check if it converged fast then duplicate the time step
		if(iter<myTissue.max_iter/2 && time_step_doublings>0){
			std::cout<<"\n\n\nYESSSSSSS... next one with bigger time step\n\n\n";
			time_step = 2*time_step;
			time_step_doublings -= 1;
		}
		
		// ADVANCE IN TIME
		
		// nodal variables
		for(int nodei=0;nodei<myTissue.n_node;nodei++)
		{
			if(myTissue.node_rho[nodei]>0){
				myTissue.node_rho_0[nodei] = myTissue.node_rho[nodei];
			}else{
				myTissue.node_rho_0[nodei] = 0.0;
			}
			if(myTissue.node_c[nodei]>0){
				myTissue.node_c_0[nodei] = myTissue.node_c[nodei] ;
			}else{
				myTissue.node_c_0[nodei] = 0.0;
			}

		}
		// integration point variables
		for(int elemi=0;elemi<myTissue.n_quadri;elemi++)
		{
			for(int IPi=0;IPi<4;IPi++){
			//
				myTissue.ip_phic_0[elemi*4+IPi] = myTissue.ip_phic[elemi*4+IPi];
				myTissue.ip_kc_0[elemi*4+IPi] = myTissue.ip_kc[elemi*4+IPi];
				myTissue.ip_a0c_0[elemi*4+IPi] = myTissue.ip_a0c[elemi*4+IPi];
				myTissue.ip_kappac_0[elemi*4+IPi] = myTissue.ip_kappac[elemi*4+IPi];
				//
				myTissue.ip_phif_0[elemi*4+IPi] = myTissue.ip_phif[elemi*4+IPi];
				myTissue.ip_kf_0[elemi*4+IPi] = myTissue.ip_kf[elemi*4+IPi];
				myTissue.ip_a0f_0[elemi*4+IPi] = myTissue.ip_a0f[elemi*4+IPi];
				myTissue.ip_kappaf_0[elemi*4+IPi] = myTissue.ip_kappaf[elemi*4+IPi];
				//
				myTissue.ip_lamdaP_0[elemi*4+IPi] = myTissue.ip_lamdaP[elemi*4+IPi];
			}
		}
		
		time += time_step;
		myTissue.time = time;
		myTissue.time_step = time_step;
		if(time>time_final){break;}
		std::cout<<"End of Newton increments, iter: "<<iter<<", residual: "<<residuum<<"\nEnd of time step :"<<step<<", \nTime: "<<time<<"\n\n";
		
		// write out a paraview file.
		if(step%save_freq==0)	
		{
			std::stringstream ss;
			ss << int(time/min_time_step) ;
			std::string filename_step = filename + ss.str()+".vtk";
			std::string filename_step_tissue = filename + ss.str()+".txt";
			writeParaview(myTissue,filename_step.c_str(),residuum);
			writeTissue(myTissue,filename_step_tissue.c_str(),time,residuum);
		}
		
		// write out node variables in a file
		for(int nodei=0;nodei<save_node.size();nodei++){
			std::stringstream ss;
			ss << save_node[nodei];
			std::string filename_nodei = filename +"node"+ ss.str()+".txt";
			if(step==0){
				std::ofstream savefile(filename_nodei.c_str());
				if (!savefile) {throw std::runtime_error("Unable to open output file.");}
				savefile<<"## SAVING NODE "<<save_node[nodei]<<"TIME X(0) X(1) RHO C\n";
				savefile.close();
			}
			writeNode(myTissue,filename_nodei.c_str(),save_node[nodei],time);
		}
		// write out iP variables in a file
		for(int ipi=0;ipi<save_ip.size();ipi++){
			std::stringstream ss;
			ss << save_ip[ipi];
			std::string filename_ipi = filename + "IP"+ss.str()+".txt";
			if(step==0){
				std::ofstream savefile(filename_ipi.c_str());
				if (!savefile) {throw std::runtime_error("Unable to open output file.");}
				savefile<<"## SAVING IP "<<save_node[ipi]<<"TIME phi a0(0) a0(1) kappa lamdaN(0) lamdaB(1)\n";
				savefile.close();
			}
			writeIP(myTissue,filename_ipi.c_str(),save_ip[ipi],time);
		}

	}
	// FINISH TIME LOOP
}


//---------------------------------------//
// SPARSE LOAD SOLVER
//---------------------------------------//
//
// NOTE: 
// the mechanics part is highly non-linear so I'll use some load stepping
void sparseLoadSolver(tissue &myTissue, std::string filename)
{
	int n_dof = myTissue.n_dof_xx;
	std::cout<<"I will solve a small system of "<<n_dof<<" dof\n";
    VectorXd RR(n_dof);
	VectorXd SOL(n_dof);SOL.setZero();
    //SpMat KK(n_dof,n_dof); // sparse to solve with BiCG
    SparseMatrix<double, ColMajor> KK2(n_dof,n_dof);
	std::vector<T> KK_triplets;KK_triplets.clear();
	SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> >   solver2;
	//std::cout<<"start parameters\n";
	// PARAMETERS FOR THE SIMULATION
	
	// Save an original configuration file
	std::stringstream ss;
	ss << "REFMECH";
	std::string filename_step = filename + ss.str()+".vtk";
	//std::cout<<"write paraview\n";
	writeParaview(myTissue,filename_step.c_str(),0);
	//std::cout<<"declare variables for the solver\n";
	
	//------------------------------------//
	// DECLARE VARIABLES YOU'LL USE LATER
	// nodal positions for the element
	std::vector<Vector2d> node_x_ni;
	//			
	// concentration and cells for previous and current time
	std::vector<double> node_rho_0_ni;
	std::vector<double> node_c_0_ni;
	//			
	// values of the structural variables at the IP
	//
	std::vector<double> ip_phic_0_pi;
	std::vector<double> ip_kc_0_pi;
	std::vector<Vector2d> ip_a0c_0_pi;
	std::vector<double> ip_kappac_0_pi;
	//
	std::vector<double> ip_phif_0_pi;
	std::vector<double> ip_kf_0_pi;
	std::vector<Vector2d> ip_a0f_0_pi;
	std::vector<double> ip_kappaf_0_pi;
	//	
	std::vector<Vector2d> ip_lamdaP_0_pi;
	//
	//
    // pieces of the Residuals
    VectorXd Re_x(8);
	//
    // pieces of the Tangents
	MatrixXd Ke_x_x(8,8);
	//
	//------------------------------------//
	

	// LOOP OVER TIME
	std::cout<<"start load stepping\n";
	double load_final = 1.0;
	double load_step  = myTissue.load_step;
	double load = 0.0;
	int max_load_step_doublings = myTissue.max_load_step_doublings;
	double min_load_step = load_step/pow(2,max_load_step_doublings);
	int max_steps = 1000;
	int load_step_doublings = 0;
	double flag_res = 1;
	double flag_res0 = 1;
	double flag_incr = 1;
	double flag_normRR = 1;
	for(int step=0;step<max_steps;step++)
	{
		// GLOBAL NEWTON-RAPHSON ITERATION
		int iter = 0;
		double residuum  = 1.;
		double residuum0 = 1.;
		// create a copy of the nodal positions
		std::vector<Vector2d> node_x_copy = myTissue.node_x;
		//std::cout<<"tissue tolerance: "<<myTissue.tol<<"\n";
		//std::cout<<"max iterations: "<<myTissue.max_iter<<"\n";
		while(residuum>myTissue.tol && iter<myTissue.max_iter)
		{
            // reset the solvers
            KK2.setZero();
            RR.setZero();
            KK_triplets.clear();
            SOL.setZero();
            
            // START LOOP OVER ELEMENTS
            int n_elem = myTissue.LineQuadri.size();
            for(int ei=0;ei<n_elem;ei++)
            {
            	// element stuff
            	
            	// connectivity of the linear elements
            	std::vector<int> elem_ei = myTissue.LineQuadri[ei];
            	//std::cout<<"element "<<ei<<": "<<elem_ei[0]<<","<<elem_ei[1]<<","<<elem_ei[2]<<","<<elem_ei[3]<<"\n";
            	
				// nodal positions for this element
				node_x_ni.clear();
				
				// concentration and cells for previous and current time
				node_rho_0_ni.clear();
				node_c_0_ni.clear();
				
				// values of the structural variables at the IP
				ip_phic_0_pi.clear();
				ip_kc_0_pi.clear();
				ip_a0c_0_pi.clear();
				ip_kappac_0_pi.clear();
				//
				ip_phif_0_pi.clear();
				ip_kf_0_pi.clear();
				ip_a0f_0_pi.clear();
				ip_kappaf_0_pi.clear();
				//
				ip_lamdaP_0_pi.clear();
				//
	
				for(int ni=0;ni<4;ni++){
					// deformed positions
					node_x_ni.push_back(myTissue.node_x[elem_ei[ni]]);
					//std::cout<<"pushing node "<<elem_ei[ni]<<": "<<node_x_ni[ni](0)<<","<<node_x_ni[ni](1)<<"\n";
					
					// cells and chemical
					node_rho_0_ni.push_back(myTissue.node_rho_0[elem_ei[ni]]);
					node_c_0_ni.push_back(myTissue.node_c_0[elem_ei[ni]]);
					
					// structural variables
					// conveniently inside this loop because n_ip = n_node
					ip_phic_0_pi.push_back(myTissue.ip_phic_0[ei*4+ni]);
					
					//
					ip_kc_0_pi.push_back(myTissue.ip_kc_0[ei*4+ni]);
					
					//
					ip_phif_0_pi.push_back(myTissue.ip_phif_0[ei*4+ni]);
					
					//
					ip_kf_0_pi.push_back(myTissue.ip_kf_0[ei*4+ni]);
					
					//
					ip_a0c_0_pi.push_back(myTissue.ip_a0c_0[ei*4+ni]);
					
					//
					ip_a0f_0_pi.push_back(myTissue.ip_a0f_0[ei*4+ni]);
					
					//
					ip_kappac_0_pi.push_back(myTissue.ip_kappac_0[ei*4+ni]);
					
					//
					ip_kappaf_0_pi.push_back(myTissue.ip_kappaf_0[ei*4+ni]);
					
					//
					ip_lamdaP_0_pi.push_back(myTissue.ip_lamdaP_0[ei*4+ni]);
					
				}
				
            	// and calculate the element Re and Ke
            	// pieces of the Residuals
            	Re_x.setZero();
            	
            	// pieces of the Tangents
				Ke_x_x.setZero();
				
            	// subroutines to evaluate the element
            	//
            	//std::cout<<"wound at element "<<ei<<"\n";
            	evalMechanics(
					load_step, load,
					myTissue.elem_jac_IP[ei],
					myTissue.global_parameters,myTissue.local_parameters,
					node_rho_0_ni,node_c_0_ni,
					ip_phic_0_pi,ip_kc_0_pi,ip_a0c_0_pi,ip_kappac_0_pi,
					ip_phif_0_pi,ip_kf_0_pi,ip_a0f_0_pi,ip_kappaf_0_pi,
					ip_lamdaP_0_pi,
					node_x_ni,
					Re_x, Ke_x_x);
            	//std::cout<<"Re_X\n";
            	//std::cout<<Re_x;
				//std::cout<<"Ke_x_x\n"<<Ke_x_x<<"\n";
				
            	//std::cout<<"done with  wound\n";
            	// assemble into KK triplets array and RR

            	// LOOP OVER NODES
				for(int nodei=0;nodei<4;nodei++){
					// ASSEMBLE DISPLACEMENT RESIDUAL AND TANGENTS
					for(int coordi=0;coordi<2;coordi++){
						if(myTissue.dof_fwd_map_xx[elem_ei[nodei]*2+coordi]>-1){
							// residual
							RR(myTissue.dof_fwd_map_xx[elem_ei[nodei]*2+coordi]) += Re_x(nodei*2+coordi);
							// loop over displacement dof for the tangent
							for(int nodej=0;nodej<4;nodej++){
								for(int coordj=0;coordj<2;coordj++){
									if(myTissue.dof_fwd_map_xx[elem_ei[nodej]*2+coordj]>-1){
										T K_x_x_nici_njcj = {myTissue.dof_fwd_map_xx[elem_ei[nodei]*2+coordi],myTissue.dof_fwd_map_xx[elem_ei[nodej]*2+coordj],Ke_x_x(nodei*2+coordi,nodej*2+coordj)};
										KK_triplets.push_back(K_x_x_nici_njcj);
									}
								}
							}
						}
					}
				}
				// FINISH LOOP OVER NODES (for assembly)
			}
			// FINISH LOOP OVER ELEMENTS
			
			// residual norm
			double normRR = sqrt(RR.dot(RR));
			//std::cout<<"RR\n"<<RR<<"\n";
			//break;
			if(iter==0){
				//std::cout<<"first residual\n"<<RR<<"\nRe_rho\n"<<Re_rho<<"\nRe_c\n"<<Re_c<<"\n";
				residuum0 = normRR;
				flag_res0 = residuum0;
				if(residuum0<myTissue.tol){std::cout<<"no need to solve?: "<<residuum0<<"\n";break;}
				//std::cout<<"first tangents\nKe_x_x\n"<<Ke_x_x<<"\nKe_x_rho\n"<<Ke_x_rho<<"\nKe_x_c\n"<<Ke_x_c<<"\n";
				//std::cout<<"first tangents\nKe_rho_x\n"<<Ke_rho_x<<"\nKe_rho_rho\n"<<Ke_rho_rho<<"\nKe_rho_c\n"<<Ke_rho_c<<"\n";
				//std::cout<<"first tangents\nKe_c_x\n"<<Ke_c_x<<"\nKe_c_rho\n"<<Ke_c_rho<<"\nKe_c_c\n"<<Ke_c_c<<"\n";
			}
			else{residuum = normRR/(1+residuum0);}
			flag_res = residuum;
			flag_normRR = normRR;
			
			// SOLVE: one approach
			//std::cout<<"solve\n";
			//KK.setFromTriplets(KK_triplets.begin(), KK_triplets.end());
			KK2.setFromTriplets(KK_triplets.begin(), KK_triplets.end());
			KK2.makeCompressed();
			//std::cout<<"KK2\n"<<KK2<<"\n";
			//solver2.analyzePattern(KK2); 
			// Compute the numerical factorization 
			//solver2.factorize(KK2); 
			solver2.compute(KK2);
			//Use the factors to solve the linear system 
			SOL = solver2.solve(-1.*RR); 
			
			// SOLVE: alternate
			//BiCGSTAB<SparseMatrix<double> > solver;
			//solver.compute(KK2);
			//VectorXd SOL = solver.solve(-RR);
			//std::cout << "#iterations:     " << solver.iterations() << std::endl;
  			//std::cout << "estimated error: " << solver.error()      << std::endl;
			//std::cout<<SOL2<<"\n";
			
			// update the solution
			double normSOL = sqrt(SOL.dot(SOL));
			flag_incr = normSOL;
			residuum = std::min(residuum,normRR);
			residuum = std::min(residuum,normSOL);
			for(int dofi=0;dofi<n_dof;dofi++)
			{
				int dof_inv_i = myTissue.dof_inv_map_xx[dofi];
				int nodei  = dof_inv_i/2;
				int coordi = dof_inv_i%2;
				myTissue.node_x[nodei](coordi)+=SOL(dofi);
			}
			iter += 1;
			if(iter == myTissue.max_iter){
				if(load_step_doublings<max_load_step_doublings){
					std::cout<<"\n\n\nNOOOOOOOOO... restart with smaller load step\n";
					iter = 0;
					residuum0 = 1.0;
					residuum = 1.0;
					load = load - load_step;
					load_step = 0.5*load_step;
					load = load+load_step;
					load_step_doublings += 1;	
					myTissue.node_x = node_x_copy;
					std::cout<<"load: "<<load<<"\n\n\n";
				}else{
					std::cout<<"\n\n\nCRAAAAAAAAP... \n\n\n";
				}
			}
			//std::cout<<"End of iteration : "<<iter<<",\nResidual before increment: "<<normRR<<"\nIncrement norm: "<<normSOL<<"\n\n";
		}
		// FINISH WHILE LOOP OF NEWTON INCREMENTS
		// check if it converged fast then duplicate the time step
		
		load += load_step;
		myTissue.load = load;
		myTissue.load_step = load_step;
		
		std::cout<<"End of Newton increments, iter: "<<iter<<", residual: "<<flag_normRR<<"\nEnd of load step :"<<load_step<<", \nLoad: "<<load<<"\n\n";
		if(load>load_final){load=1;myTissue.load = 1;break;}
	}
	// FINISH LOAD LOOP
	std::stringstream ss2;
	ss2 << "LOAD" ;
	filename_step = filename + ss2.str()+".vtk";
	std::string filename_step_tissue = filename + ss2.str()+".txt";
	writeParaview(myTissue,filename_step.c_str(),flag_res);
	writeTissue(myTissue,filename_step_tissue.c_str(),load,flag_res);
		
}


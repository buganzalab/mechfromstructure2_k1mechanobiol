/* 
	Constitutive Equations

	For the mechanics
*/
//
//------------------------------------------------------------------------------------//
// We represent the passive tissue stress using Holzapfel-Gasser-Ogden (HGO) hyperelasticity
// * the non-fibrous matrix is described by a Neo-Hookean model of shear modulus mu0
// * the fibrous matrix (collagen) is characterized by the parameters kc and k2c
// * according to the HGO model, fibers only respond to extensile loads
//------------------------------------------------------------------------------------//
//
#include "mechanics.h"
#include <vector>
#include <map>
#include <math.h>
#include <algorithm>
#include <iostream>
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
using namespace Eigen;

//------------------------------------------------------------------------------------//
// 	biology of global variables
//------------------------------------------------------------------------------------//

// for the flux function
// This defines the passive (2nd PK) stress term
Matrix2d evalSS_pas(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c,
	double phic, double mu, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double> &parameters)
{

	// parameters
	double mu0 = mu;
	double k2c = parameters[1];
	double k2f = parameters[2]; // this is not used for now since we are only modeling collagen

	// re-compute basis a0, s0
	Matrix2d CC = FF.transpose()*FF;
	Matrix2d CCinv = CC.inverse();
	Matrix2d Identity = Matrix2d::Identity(2,2);
	double lamdaP_a = lamdaP(0);
	double lamdaP_s = lamdaP(1);
	//
	Matrix2d Rot90;Rot90<<0.,-1.,1.,0.;
	Vector2d s0c = Rot90*a0c;
	Vector2d s0f = Rot90*a0f; // this is not used for now since we are only modeling collagen
	// fiber tensor in the reference
	Matrix2d a0ca0c = a0c*a0c.transpose();
	Matrix2d s0cs0c = s0c*s0c.transpose();
	//
	// recompute split. The contraction occurs with respect to the collagen direction
	Matrix2d FFg = lamdaP_a*(a0ca0c) + lamdaP_s*(s0cs0c);
	double thetaP = lamdaP_a*lamdaP_s;
	Matrix2d FFginv = (1./lamdaP_a)*(a0ca0c) + (1./lamdaP_s)*(s0cs0c);
	Matrix2d FFe = FF*FFginv;
	//
	// elastic strain
	Matrix2d CCe = FFe.transpose()*FFe;
	double I1e2D = CCe(0,0) + CCe(1,1);
	double I4ce2D = a0c.dot(CCe*a0c);
	//
	// now consider full 3D case
	Matrix3d Identity3D = Matrix3d::Identity(3,3);
	// fiber tensors
	Vector3d a0c3D; a0c3D<<a0c(0),a0c(1),0.0;
	Vector3d s0c3D; s0c3D<<s0c(0),s0c(1),0.0;
	Matrix3d a0ca0c3D = a0c3D*a0c3D.transpose();
	Matrix3d s0cs0c3D = s0c3D*s0c3D.transpose();
	// CC and invariants
	double CCe33 = 1./CCe.determinant();
	Matrix3d CCe3D;CCe3D<<CCe(0,0),CCe(0,1),0.,CCe(1,0),CCe(1,1),0.,0.,0.,CCe33;
	Matrix3d CCe3Dinv = CCe3D.inverse();
	double CCe33inv = CCe3Dinv(2,2);
	double I1e3D = CCe3D(0,0) + CCe3D(1,1) + CCe3D(2,2);
	double I4ce3D = I4ce2D;
	//
	// PK2 stress tensor under plane stress assumption (Sigma_33 = 0)
	// matrix contribution
	Matrix3d PK2e_m = mu0 * (Identity3D - 2./3. * I1e3D * CCe3Dinv);
	// fiber contribution including Macauley brackets
	double Ebar = kappac * (I1e3D - 3.) + (1. - 3. * kappac) * (I4ce3D - 1.);
	if ( Ebar < 0.0 )
		Ebar = 0.0;
	double Psic_mod = kc / (2. * k2c) * exp( k2c*pow(Ebar,2) );
	Matrix3d PK2e_f = 4. * k2c * Ebar * Psic_mod * (kappac * Identity3D + (1. - 3. * kappac) * a0ca0c3D - 2./3. * (kappac * I1e3D + (1. - 3. * kappac) * I4ce3D) * CCe3Dinv);
	// // volumetric term
	Matrix3d PK2e_v = (2./3. * I1e3D - 1. / CCe33inv) * (mu0 + 2. * kappac * kc * (kappac * I1e3D - 1.) * exp( k2c * pow(kappac * I1e3D - 1. ,2) ) ) * CCe3Dinv;
	//
	// get 3D PK2 stress matrix
	Matrix3d PK2e3D_pas = PK2e_m + PK2e_f + PK2e_v;
	// slice down to 2D matrix
	Matrix2d PK2e2D_pas; PK2e2D_pas<<PK2e3D_pas(0,0),PK2e3D_pas(0,1),PK2e3D_pas(1,0),PK2e3D_pas(1,1);
	// pull back the whole 2D thing to the reference
	Matrix2d SS = thetaP*FFginv*PK2e2D_pas*FFginv;

	// // St venant kirchhoff to check convergence of a linear material
	// // St Venant Kirchhoff is linear for a description in the reference
	// // i.e. in terms of S and E rather than linear in sigma and epsilon
	// // Not good for large deformations, but good to check convergence
	
	// double nu = 0.4;
	// double mu0 = 0.1;
	// double Ym = mu0;
	// Matrix3d SVK; SVK.setZero(); SVK<<1,nu,0,nu,1,0,0,0,(1-nu)/2.;
	// SVK = mu0/(1.0-nu*nu)*SVK;
	// Matrix2d EE = 0.5*(CC-Identity);
	// Vector3d EEvoigt; EEvoigt.setZero(); EEvoigt<<EE(0,0),EE(1,1),2*EE(0,1);
	// Vector3d SSvoigt = SVK*EEvoigt;
	// SS(0,0) = SSvoigt(0);
	// SS(1,1) = SSvoigt(1);
	// SS(0,1) = SSvoigt(2);
	// SS(1,0) = SSvoigt(2);
	// std::cout<<"CC matrix\n"<<CC<<"\n";
	// std::cout<<"EE matrix\n"<<EEvoigt<<"\n";
	// std::cout<<"SVK\n"<<SVK<<"\n";
	
	return SS;
}

// This computes the numerical tangent of the passive stress term
Matrix3d eval_DDpas(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c,
	double phic, double mu, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double> &parameters)
{
	Matrix3d DDpas;DDpas.setZero();
	// Mechanics
	// numerical
	Matrix2d CC = FF.transpose()*FF;
	EigenSolver<Matrix2d> es(CC);
	// then polar decomposition is
	std::vector<Vector2d> Ubasis(2,Vector2d(0.0,0.0));
	Matrix2d CC_eiv = es.eigenvectors().real();
	Ubasis[0] =  CC_eiv.col(0);
	Ubasis[1] =  CC_eiv.col(1);
	Vector2d Clamda =  es.eigenvalues().real();
	// Build the U matrix from basis and the root of the eigenvuales
	Matrix2d UU = sqrt(Clamda(0))*Ubasis[0]*Ubasis[0].transpose() + sqrt(Clamda(1))*Ubasis[1]*Ubasis[1].transpose() ;
	Matrix2d RR = FF*UU.inverse();
	Matrix2d FF_plus,FF_minus;
	Matrix2d CC_plus,CC_minus;
	Matrix2d UU_plus,UU_minus;
	std::vector<Vector2d> Ebasis(2,Vector2d(0.0,0.0));
	Ebasis[0] = Vector2d(1.0,0.0);
	Ebasis[1] = Vector2d(0.0,1.0);
	std::vector<Vector2d> Ubasis_plus(2,Vector2d(0.0,0.0));
	std::vector<Vector2d> Ubasis_minus(2,Vector2d(0.0,0.0));
	Matrix2d CC_eiv_plus,CC_eiv_minus;
	Vector2d Clamda_plus,Clamda_minus;
	//
	Matrix2d SS_plus,SS_minus;
	//

	double epsilon = 1e-9;

	//
	Vector3d voigt_table_I_i(0,1,0);
	Vector3d voigt_table_I_j(0,1,1);
	Vector3d voigt_table_J_k(0,1,0);
	Vector3d voigt_table_J_l(0,1,1);
	int ii,jj,kk,ll;
	for(int II=0;II<3;II++){
		for(int JJ=0;JJ<3;JJ++){
			ii = voigt_table_I_i(II);
			jj = voigt_table_I_j(II);
			kk = voigt_table_J_k(JJ);
			ll = voigt_table_J_l(JJ);

			CC_plus = CC + epsilon*(Ebasis[kk]*Ebasis[ll].transpose())+ epsilon*(Ebasis[ll]*Ebasis[kk].transpose());
			CC_minus = CC - epsilon*(Ebasis[kk]*Ebasis[ll].transpose())- epsilon*(Ebasis[ll]*Ebasis[kk].transpose());

			// Polar decomposition of CCplus and CCminus
			EigenSolver<Matrix2d> esp(CC_plus);
			EigenSolver<Matrix2d> esm(CC_minus);
			Matrix2d CC_eiv_plus = esp.eigenvectors().real();
			Matrix2d CC_eiv_minus = esm.eigenvectors().real();
			Ubasis_plus[0] =  CC_eiv_plus.col(0);
			Ubasis_plus[1] =  CC_eiv_plus.col(1);
			Ubasis_minus[0] =  CC_eiv_minus.col(0);
			Ubasis_minus[1] =  CC_eiv_minus.col(1);
			Clamda_plus =  esp.eigenvalues().real();
			Clamda_minus =  esm.eigenvalues().real();
			UU_plus = sqrt(Clamda_plus(0))*Ubasis_plus[0]*Ubasis_plus[0].transpose() + sqrt(Clamda_plus(1))*Ubasis_plus[1]*Ubasis_plus[1].transpose() ;
			UU_minus = sqrt(Clamda_minus(0))*Ubasis_minus[0]*Ubasis_minus[0].transpose() + sqrt(Clamda_minus(1))*Ubasis_minus[1]*Ubasis_minus[1].transpose() ;
			FF_plus = RR*UU_plus;
			FF_minus = RR*UU_minus;

			SS_plus =  evalSS_pas(FF_plus,rho,Grad_rho,c,Grad_c,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,parameters);
			SS_minus =  evalSS_pas(FF_minus,rho,Grad_rho,c,Grad_c,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,parameters);

			DDpas(II,JJ) =  (1.0/(4.0*epsilon))*(SS_plus(ii,jj) - SS_minus(ii,jj) );
		}
	}
	return DDpas;
}

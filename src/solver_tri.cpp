/*
	Pre and Post processing functions
	Solver functions
	Struct and classes for the problem definition
*/


#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <math.h>
#include "wound_tri.h"
#include "solver_tri.h"
#include "utilities_tri.h"
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
#include <Eigen/Sparse> // functions for solution of linear systems
#include <Eigen/OrderingMethods>
//#include<Eigen/StdVector> // to deal with std::vector<Vector2d>

typedef Eigen::SparseMatrix<double> SpMat; // declares a column-major sparse matrix type of double
typedef Eigen::Triplet<double> T;

using namespace Eigen;

//------------------------------------------------------------------------------------//
// 2nd LINK BETWEEN TISSUE STRUCTURE AND MECHANICAL PROPERTIES:
// * mu_0 is proportional to fibrin content (wound only)
//		-> \mu_0^W = 2.0 * \hat{C_{10}^W} * \phi_f^W
// * k1 depends on collagen content and crosslinking (in the entire tissue) 
//		-> k_1 = \hat{k_1} * \xi_c^exp_csi * \phi_c
//------------------------------------------------------------------------------------//


//-------------------------------------------------------------------------------------//
// PRE PROCESSING
//-------------------------------------------------------------------------------------//


//---------------------------------------//
// FILL IN DOF MAPS
//---------------------------------------//
// NOTE: the structure has an empty dof map, but it already has the eBC, the essential
// boundary condition maps
void fillDOFmap(tissue &myTissue)
{
	// some mesh values
	int n_node = myTissue.node_X.size();
	int n_elem  = myTissue.Tri.size();

	// initialize the dof count and the maps
	int dof_count = 0;
	int dof_count_xx = 0;
	int dof_count_p = 0;
	// displacements
	std::vector< int > dof_fwd_map_x(n_node*2,-1);
	std::vector< int > dof_fwd_map_xx(n_node*2,-1);
	// concentrations
	std::vector< int > dof_fwd_map_rho(n_node,-1); // fibroblasts 
	std::vector< int > dof_fwd_map_c(n_node,-1); // macrophage/TGFb1 
	std::vector< int > dof_fwd_map_p(n_node,-1); // neutrophil/PDFG

	// all dof inverse map
	std::vector< std::vector<int> > dof_inv_map;
	std::vector<int> dof_inv_map_xx;
	std::vector<int> dof_inv_map_p;

	// loop over the node set
	for(int i=0;i<n_node;i++)
	{
		// check if node has essential boundary conditions for displacements
		for(int j=0; j<2; j++)
		{
			if(myTissue.eBC_x.find(i*2+j)==myTissue.eBC_x.end())
			{
				// no eBC_x, means this is a DOF
				// fill in forward map
				dof_fwd_map_x[i*2+j] = dof_count;
				dof_fwd_map_xx[i*2+j] = dof_count_xx;
				// fill in inverse map
				std::vector<int> dofinvx = {0,i*2+j};
				dof_inv_map.push_back(dofinvx);
				dof_inv_map_xx.push_back(i*2+j);
				dof_count+=1;
				dof_count_xx+=1;
			}else{
				// this node is in fact in the eBC
				myTissue.node_x[i](j) = myTissue.node_X[i](j) + myTissue.eBC_x.find(i*2+j)->second;
			}
		}
		if(myTissue.eBC_rho.find(i)==myTissue.eBC_rho.end())
		{
			dof_fwd_map_rho[i] = dof_count;
			std::vector<int> dofinvrho = {1,i};
			dof_inv_map.push_back(dofinvrho);
			dof_count+=1;
		}else{
			// this node is in fact in the eBC,
			myTissue.node_rho[i] = myTissue.eBC_rho.find(i)->second;
		}
		if(myTissue.eBC_c.find(i)==myTissue.eBC_c.end())
		{
			dof_fwd_map_c[i] = dof_count;
			std::vector<int> dofinvc = {2,i};
			dof_inv_map.push_back(dofinvc);
			dof_count+=1;
		}else{
			// this node is in fact in the eBC,
			myTissue.node_c[i] = myTissue.eBC_c.find(i)->second;
		}

		// precursor is solved separately since there is no back coupling
		if(myTissue.eBC_p.find(i)==myTissue.eBC_p.end())
		{
			dof_fwd_map_p[i] = dof_count_p;
			dof_inv_map_p.push_back(i);
			dof_count_p+=1;
		}else{
			// this node is in fact in the eBC,
			myTissue.node_p[i] = myTissue.eBC_p.find(i)->second;
		}

	}
	myTissue.dof_fwd_map_x = dof_fwd_map_x;
	myTissue.dof_fwd_map_rho = dof_fwd_map_rho;
	myTissue.dof_fwd_map_c = dof_fwd_map_c;
	myTissue.dof_inv_map = dof_inv_map;
	myTissue.n_dof = dof_count;
	myTissue.n_dof_xx = dof_count_xx;
	myTissue.n_dof_p = dof_count_p;
	myTissue.dof_fwd_map_xx = dof_fwd_map_xx;
	myTissue.dof_inv_map_xx = dof_inv_map_xx;
	myTissue.dof_fwd_map_p = dof_fwd_map_p;
	myTissue.dof_inv_map_p = dof_inv_map_p;
}


//---------------------------------------//
// EVAL JACOBIANS
//---------------------------------------//

// NOTE: assume the mesh and boundary conditions have already been read. The following
// function stores the internal element variables, namely the Jacobians, maybe some other
// thing, but the Jacobians is the primary thing
//
// EVAL JACOBIANS
void evalElemJacobians(tissue &myTissue)
{
	// clear the vectors
	//std::vector<Matrix2d, aligned_allocator<Matrix2d> > elem_jac_IP;
	std::vector<Matrix2d> elem_jac_IP;
	// loop over the elements
	int n_elem = myTissue.Tri.size();
	std::cout<<"evaluating element jacobians, over "<<n_elem<<" elements\n";
	for(int ei=0;ei<n_elem;ei++)
	{
		// this element connectivity
		std::vector<int> elem = myTissue.Tri[ei];
		// nodal positions for this element
		std::vector<Vector2d> node_X_ni;
		for(int ni=0;ni<3;ni++)
		{
			node_X_ni.push_back(myTissue.node_X[elem[ni]]);
		}
		// compute jacobian (single integration point)
		Matrix2d jac_IPi = evalJacobian(node_X_ni);
		elem_jac_IP.push_back(jac_IPi);
	}
	// assign to the structure
	myTissue.elem_jac_IP = elem_jac_IP;
}

//---------------------------------------//
// EVAL DEFORMATION GRADIENTS
//---------------------------------------//

void evalElemStressStrain(tissue &myTissue)
{
	// clear the vectors
	myTissue.ip_FF.clear();
	myTissue.ip_sig_all.clear();
	
	// aux variable
	Matrix2d FF, sigma;
	Vector2d EE_eiv, SS_eiv;
	// loop over the elements
	int n_elem = myTissue.Tri.size();
	for(int ei=0;ei<n_elem;ei++)
	{

		// this element connectivity
		std::vector<int> elem = myTissue.Tri[ei];

		// nodal positions for this element
		std::vector<Vector2d> node_x_ni; node_x_ni.clear();
		std::vector<Vector2d> node_X_ni; node_X_ni.clear();
		std::vector<double> node_rho_0_ni;node_rho_0_ni.clear();
		std::vector<double> node_c_0_ni;node_c_0_ni.clear();
		// note, the precursor is not used here 

		for(int ni=0;ni<3;ni++)
		{
			node_x_ni.push_back(myTissue.node_x[elem[ni]]);
			node_X_ni.push_back(myTissue.node_X[elem[ni]]);
			node_rho_0_ni.push_back(myTissue.node_rho_0[elem[ni]]);
			node_c_0_ni.push_back(myTissue.node_c_0[elem[ni]]);
		
		}		

		//evalDeformationGradient(myTissue.elem_jac_IP[ei],node_x_ni,FF);
		
		evalStrainsStress(myTissue.elem_jac_IP[ei], myTissue.global_parameters,myTissue.local_parameters,
			node_rho_0_ni, node_c_0_ni, myTissue.ip_phic_0[ei],myTissue.ip_mu_0[ei],myTissue.ip_kc_0[ei],myTissue.ip_a0c_0[ei],myTissue.ip_kappac_0[ei],
			myTissue.ip_phif_0[ei],myTissue.ip_kf_0[ei],myTissue.ip_a0f_0[ei],myTissue.ip_kappaf_0[ei],
			myTissue.ip_lamdaP_0[ei],node_x_ni,EE_eiv, SS_eiv, FF, sigma);

		myTissue.ip_FF.push_back(FF);
		myTissue.ip_sig_all.push_back(sigma);
	}
}


//-------------------------------------------------------------------------------------//
// SOLVER
//-------------------------------------------------------------------------------------//


//---------------------------------------//
// SPARSE SOLVER
//---------------------------------------//
//
// NOTE: at this point the struct is ready with all that is needed, including boundary and
// initial conditions. Also, the internal constants, namely the Jacobians, have been
// calculated and stored. Time to solve the global system
void sparseWoundSolver(tissue &myTissue, std::string filename, double save_freq,const std::vector<int> &save_node,const std::vector<int> &save_ip)
{
	int n_dof = myTissue.n_dof;
	std::cout<<"I will solve a small system of "<<n_dof<<" dof\n";
    VectorXd RR(n_dof);
	VectorXd SOL(n_dof);SOL.setZero();
    //SpMat KK(n_dof,n_dof); // sparse to solve with BiCG
    SparseMatrix<double, ColMajor> KK2(n_dof,n_dof);
	std::vector<T> KK_triplets;KK_triplets.clear();
	SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> >   solver2;
	// PARAMETERS FOR THE SIMULATION
	double time_final = myTissue.time_final;
	double time_step  = myTissue.time_step;
	double local_time_step = myTissue.local_time_step;
	double time = myTissue.time;
	int save_step = 1;
	int max_time_step_doublings = myTissue.max_time_step_doublings;
	double min_time_step = time_step/pow(2,max_time_step_doublings);

	// Save an original configuration file
	std::stringstream ss;
	ss << "REF";
	std::string filename_step_Biol = filename + ss.str()+"_Biol.vtk";
	std::string filename_step_Mech = filename + ss.str()+"_Mech.vtk";
	writeParaviewBiol(myTissue,filename_step_Biol.c_str(),1);
	writeParaviewMech(myTissue,filename_step_Mech.c_str(),1);

	//------------------------------------//
	// DECLARE VARIABLES YOU'LL USE LATER
	// deformed nodal positions for the element
	std::vector<Vector2d> node_x_ni;
	//
	// concentration and cells for previous and current time
	std::vector<double> node_rho_0_ni;
	std::vector<double> node_c_0_ni;
	std::vector<double> node_rho_ni;
	std::vector<double> node_c_ni;
	// precursor 
	std::vector<double> node_p_0_ni;
	std::vector<double> node_p_ni;
	//
	// values of the structural variables at the IP
	//
	double ip_phic_0_pi;
	double ip_mu_0_pi;
	double ip_kc_0_pi;
	Vector2d ip_a0c_0_pi;
	double ip_kappac_0_pi;
	//
	double ip_phif_0_pi;
	double ip_kf_0_pi;
	Vector2d ip_a0f_0_pi;
	double ip_kappaf_0_pi;
	//
	Vector2d ip_lamdaP_0_pi;
	//
	double ip_phic_pi;
	double ip_mu_pi;
	double ip_kc_pi;
	Vector2d ip_a0c_pi;
	double ip_kappac_pi;
	//
	double ip_phif_pi;
	double ip_kf_pi;
	Vector2d ip_a0f_pi;
	double ip_kappaf_pi;
	//
	Vector2d ip_lamdaP_pi;
	//
  	// pieces of the Residuals
  	VectorXd Re_x(6);
  	VectorXd Re_rho(3);
  	VectorXd Re_c(3);
	//
  	// pieces of the Tangents
	MatrixXd Ke_x_x(6,6);
	MatrixXd Ke_x_rho(6,3);
	MatrixXd Ke_x_c(6,3);
	MatrixXd Ke_rho_x(3,6);
	MatrixXd Ke_rho_rho(3,3);
	MatrixXd Ke_rho_c(3,3);
	MatrixXd Ke_c_x(3,6);
	MatrixXd Ke_c_rho(3,3);
	MatrixXd Ke_c_c(3,3);
	//
	// strain and stress tensors
	Matrix2d ip_FF;
	//
	//------------------------------------//

	// LOOP OVER TIME
	std::cout<<"start loop over time\n";
	int max_steps = 4e6;
	int time_step_doublings = 0;
	double flag_res = 1;
	
	for(int step=0;step<max_steps;step++){
		// GLOBAL NEWTON-RAPHSON ITERATION
		int iter = 0;
		double residuum  = 1.;
		double residuum0 = 1.;

		// store the previous nodal position
		std::vector<Vector2d> node_x_0 = myTissue.node_x;

		while(residuum>myTissue.tol && iter<myTissue.max_iter){
			// reset the solvers
		    KK2.setZero();
		    RR.setZero();
		    KK_triplets.clear();
		    SOL.setZero();

		    // START LOOP OVER ELEMENTS
		    int n_elem = myTissue.Tri.size();
		    for(int ei=0;ei<n_elem;ei++){
		      	// element stuff

		      	// connectivity of the linear elements
		      	std::vector<int> elem_ei = myTissue.Tri[ei];

				// deformed nodal positions for this element
				node_x_ni.clear();

				// concentration and cells for previous and current time
				node_rho_0_ni.clear();
				node_c_0_ni.clear();
				node_rho_ni.clear();
				node_c_ni.clear();
				node_p_0_ni.clear();

				for(int ni=0;ni<3;ni++){
					// deformed positions
					node_x_ni.push_back(myTissue.node_x[elem_ei[ni]]);

					// cells and chemical
					node_rho_0_ni.push_back(myTissue.node_rho_0[elem_ei[ni]]);
					node_c_0_ni.push_back(myTissue.node_c_0[elem_ei[ni]]);
					node_rho_ni.push_back(myTissue.node_rho[elem_ei[ni]]);
					node_c_ni.push_back(myTissue.node_c[elem_ei[ni]]);
					node_p_0_ni.push_back(myTissue.node_p_0[elem_ei[ni]]);
				}

				//hard-code link between tissue structure and mechanical properties

				// in the wound, mu0 is proportional to the fibrin content (phif)
				double C10_hat = 0.3;
				if ( myTissue.ip_kappaf_0[ei] == 1.0 )
				{
					// 2x so that C10 ~ phif, knowing that C10 = mu0/2.
					myTissue.ip_mu_0[ei] = 2.0 * C10_hat * myTissue.ip_phif_0[ei];
					myTissue.ip_mu[ei]   = 2.0 * C10_hat * myTissue.ip_phif[ei];						
				}
				// in the whole tissue, k1 is proportional to the collagen content (phic)
				double kc_hat = 1.6;
				double exp_csi = 10.;
				myTissue.ip_kc_0[ei] = kc_hat * pow(myTissue.ip_kf_0[ei],exp_csi) * myTissue.ip_phic_0[ei];
				myTissue.ip_kc[ei]   = kc_hat * pow(myTissue.ip_kf[ei],exp_csi) * myTissue.ip_phic[ei];						

				// single IP data
				ip_phic_0_pi = myTissue.ip_phic_0[ei];
				ip_mu_0_pi= myTissue.ip_mu_0[ei];
				ip_kc_0_pi= myTissue.ip_kc_0[ei];
				ip_a0c_0_pi= myTissue.ip_a0c_0[ei];
				ip_kappac_0_pi= myTissue.ip_kappac_0[ei];
				//
				ip_phif_0_pi = myTissue.ip_phif_0[ei];
				ip_kf_0_pi= myTissue.ip_kf_0[ei];
				ip_a0f_0_pi= myTissue.ip_a0f_0[ei];
				ip_kappaf_0_pi= myTissue.ip_kappaf_0[ei];
				//
				ip_lamdaP_0_pi= myTissue.ip_lamdaP_0[ei];
				//
				ip_phic_pi = myTissue.ip_phic[ei];
				ip_mu_pi= myTissue.ip_mu[ei];
				ip_kc_pi= myTissue.ip_kc[ei];
				ip_a0c_pi= myTissue.ip_a0c[ei];
				ip_kappac_pi= myTissue.ip_kappac[ei];
				//
				ip_phif_pi = myTissue.ip_phif[ei];
				ip_kf_pi= myTissue.ip_kf[ei];
				ip_a0f_pi= myTissue.ip_a0f[ei];
				ip_kappaf_pi= myTissue.ip_kappaf[ei];
				//
				ip_lamdaP_pi= myTissue.ip_lamdaP[ei];

		      	// and calculate the element Re and Ke
		      	// pieces of the Residuals
		      	Re_x.setZero();
		      	Re_rho.setZero();
		      	Re_c.setZero();

		      	// pieces of the Tangents
				Ke_x_x.setZero();
				Ke_x_rho.setZero();
				Ke_x_c.setZero();
				Ke_rho_x.setZero();
				Ke_rho_rho.setZero();
				Ke_rho_c.setZero();
				Ke_c_x.setZero();
				Ke_c_rho.setZero();
				Ke_c_c.setZero();

		      	// subroutines to evaluate the element
		      	//
		      	evalWound(  time_step,local_time_step,
							myTissue.elem_jac_IP[ei],
							myTissue.global_parameters,myTissue.local_parameters,
							node_rho_0_ni,node_c_0_ni,node_p_0_ni,
							ip_phic_0_pi,ip_mu_0_pi,ip_kc_0_pi,ip_a0c_0_pi,ip_kappac_0_pi,
							ip_phif_0_pi,ip_kf_0_pi,ip_a0f_0_pi,ip_kappaf_0_pi,
							ip_lamdaP_0_pi,
							node_rho_ni, node_c_ni,
							ip_phic_pi,ip_mu_pi,ip_kc_pi,ip_a0c_pi,ip_kappac_pi,
							ip_phif_pi,ip_kf_pi,ip_a0f_pi,ip_kappaf_pi,
							ip_lamdaP_pi, ip_FF,
							node_x_ni,
							Re_x, Ke_x_x, Ke_x_rho, Ke_x_c,
							Re_rho, Ke_rho_x, Ke_rho_rho, Ke_rho_c,
							Re_c, Ke_c_x, Ke_c_rho, Ke_c_c);

				// store the new IP values
				myTissue.ip_phic[ei] = ip_phic_pi;
				myTissue.ip_mu[ei] = ip_mu_pi;
				myTissue.ip_kc[ei] = ip_kc_pi;
				myTissue.ip_a0c[ei] = ip_a0c_pi;
				myTissue.ip_kappac[ei] = ip_kappac_pi;
				//
				myTissue.ip_phif[ei] = ip_phif_pi;
				myTissue.ip_kf[ei] = ip_kf_pi;
				myTissue.ip_a0f[ei] = ip_a0f_pi;
				myTissue.ip_kappaf[ei] = ip_kappaf_pi;
				//
				myTissue.ip_lamdaP[ei] = ip_lamdaP_pi;
		      	//
				myTissue.ip_FF[ei] = ip_FF;
			
		      	// assemble into KK triplets array and RR

		        // LOOP OVER NODES
				for(int nodei=0;nodei<3;nodei++){
					// ASSEMBLE DISPLACEMENT RESIDUAL AND TANGENTS
					for(int coordi=0;coordi<2;coordi++){
						if(myTissue.dof_fwd_map_x[elem_ei[nodei]*2+coordi]>-1){
							// residual
							RR(myTissue.dof_fwd_map_x[elem_ei[nodei]*2+coordi]) += Re_x(nodei*2+coordi);
							// loop over displacement dof for the tangent
							for(int nodej=0;nodej<3;nodej++){
								for(int coordj=0;coordj<2;coordj++){
									if(myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj]>-1){
										T K_x_x_nici_njcj = {myTissue.dof_fwd_map_x[elem_ei[nodei]*2+coordi],myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj],Ke_x_x(nodei*2+coordi,nodej*2+coordj)};
										KK_triplets.push_back(K_x_x_nici_njcj);
									}
								}
								// rho tangent
								if(myTissue.dof_fwd_map_rho[elem_ei[nodej]]>-1){
									T K_x_rho_nici_nj = {myTissue.dof_fwd_map_x[elem_ei[nodei]*2+coordi],myTissue.dof_fwd_map_rho[elem_ei[nodej]],Ke_x_rho(nodei*2+coordi,nodej)};
									KK_triplets.push_back(K_x_rho_nici_nj);
								}
								// C tangent
								if(myTissue.dof_fwd_map_c[elem_ei[nodej]]>-1){
									T K_x_c_nici_nj = {myTissue.dof_fwd_map_x[elem_ei[nodei]*2+coordi],myTissue.dof_fwd_map_c[elem_ei[nodej]],Ke_x_c(nodei*2+coordi,nodej)};
									KK_triplets.push_back(K_x_c_nici_nj);
								}
							}
						}
					}
					// ASSEMBLE RHO
					if(myTissue.dof_fwd_map_rho[elem_ei[nodei]]>-1){
						RR(myTissue.dof_fwd_map_rho[elem_ei[nodei]]) += Re_rho(nodei);
						// tangent of the rho
						for(int nodej=0;nodej<3;nodej++){
							for(int coordj=0;coordj<2;coordj++){
								if(myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj]>-1){
									T K_rho_x_ni_njcj = {myTissue.dof_fwd_map_rho[elem_ei[nodei]],myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj],Ke_rho_x(nodei,nodej*2+coordj)};
									KK_triplets.push_back(K_rho_x_ni_njcj);
								}
							}
							if(myTissue.dof_fwd_map_rho[elem_ei[nodej]]>-1){
								T K_rho_rho_ni_nj = {myTissue.dof_fwd_map_rho[elem_ei[nodei]],myTissue.dof_fwd_map_rho[elem_ei[nodej]],Ke_rho_rho(nodei,nodej)};
								KK_triplets.push_back(K_rho_rho_ni_nj);
							}
							if(myTissue.dof_fwd_map_c[elem_ei[nodej]]>-1){
								T K_rho_c_ni_nj = {myTissue.dof_fwd_map_rho[elem_ei[nodei]],myTissue.dof_fwd_map_c[elem_ei[nodej]],Ke_rho_c(nodei,nodej)};
								KK_triplets.push_back(K_rho_c_ni_nj);
							}
						}
					}
					// ASSEMBLE C
					if(myTissue.dof_fwd_map_c[elem_ei[nodei]]>-1){
						RR(myTissue.dof_fwd_map_c[elem_ei[nodei]]) += Re_c(nodei);
						// tangent of the C
						for(int nodej=0;nodej<3;nodej++){
							for(int coordj=0;coordj<2;coordj++){
								if(myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj]>-1){
									T K_c_x_ni_njcj = {myTissue.dof_fwd_map_c[elem_ei[nodei]],myTissue.dof_fwd_map_x[elem_ei[nodej]*2+coordj],Ke_c_x(nodei,nodej*2+coordj)};
									KK_triplets.push_back(K_c_x_ni_njcj);
								}
							}
							if(myTissue.dof_fwd_map_rho[elem_ei[nodej]]>-1){
								T K_c_rho_ni_nj = {myTissue.dof_fwd_map_c[elem_ei[nodei]],myTissue.dof_fwd_map_rho[elem_ei[nodej]],Ke_c_rho(nodei,nodej)};
								KK_triplets.push_back(K_c_rho_ni_nj);
							}
							if(myTissue.dof_fwd_map_c[elem_ei[nodej]]>-1){
								T K_c_c_ni_nj = {myTissue.dof_fwd_map_c[elem_ei[nodei]],myTissue.dof_fwd_map_c[elem_ei[nodej]],Ke_c_c(nodei,nodej)};
								KK_triplets.push_back(K_c_c_ni_nj);
							}
						}
					}
				}
			// FINISH LOOP OVER NODES (for assembly)
			}
			// FINISH LOOP OVER ELEMENTS

			// residual norm
			double normRR = sqrt(RR.dot(RR))/n_dof;
			if(iter==0){
				residuum0 = normRR;
				if(residuum0<myTissue.tol){std::cout<<"no need to solve?: "<<residuum0<<"\n";break;}
			}
			else{residuum = normRR/(1+residuum0);}
			flag_res = residuum;
			// SOLVE: one approach
			//KK.setFromTriplets(KK_triplets.begin(), KK_triplets.end());
			KK2.setFromTriplets(KK_triplets.begin(), KK_triplets.end());
			KK2.makeCompressed();
			//std::cout<<"KK2\n"<<KK2<<"\n";
			//solver2.analyzePattern(KK2);
			// Compute the numerical factorization
			// solver2.factorize(KK2);
			solver2.compute(KK2);
			//Use the factors to solve the linear system
			std::cout<<"residuals \n";
			std::cout<<normRR<<"\n";
			SOL = solver2.solve(-1.*RR);

			// SOLVE: alternate
			//BiCGSTAB<SparseMatrix<double> > solver;
			//solver.compute(KK2);
			//VectorXd SOL = solver.solve(-RR);
			//std::cout << "#iterations:     " << solver.iterations() << std::endl;
  			//std::cout << "estimated error: " << solver.error()      << std::endl;
			//std::cout<<SOL2<<"\n";

			// update the solution
			double normSOL = sqrt(SOL.dot(SOL));
			for(int dofi=0;dofi<n_dof;dofi++){
				std::vector<int> dof_inv_i = myTissue.dof_inv_map[dofi];
				if(dof_inv_i[0]==0){
					// displacement dof
					int nodei  = dof_inv_i[1]/2;
					int coordi = dof_inv_i[1]%2;
					myTissue.node_x[nodei](coordi)+=SOL(dofi);
				}else if(dof_inv_i[0]==1){
					// rho dof
					myTissue.node_rho[dof_inv_i[1]] += SOL(dofi);
				}else if(dof_inv_i[0]==2){
					// C dof
					myTissue.node_c[dof_inv_i[1]] += SOL(dofi);
				}
			}
			iter += 1;
			if(iter == myTissue.max_iter){
				if(time_step_doublings<max_time_step_doublings){
					std::cout<<"\n\n\nNOOOOOOOOO... restart with smaller time step\n\n\n";
					iter = 0;
					residuum0 = 1.0;
					residuum = 1.0;
					time_step = 0.5*time_step;
					time_step_doublings += 1;
					// restart the simulation
					myTissue.node_x = node_x_0;
					myTissue.node_rho = myTissue.node_rho_0;
					myTissue.node_c = myTissue.node_c_0;
				}else{
					std::cout<<"\n\n\nCRAAAAAAAAP... \n\n\n";
				}
			}
			std::cout<<"End of iteration : "<<iter<<",\nResidual before increment: "<<normRR<<"\nIncrement norm: "<<normSOL<<"\n\n";
		}

		// FINISH WHILE LOOP OF NEWTON INCREMENTS
		// check if it converged fast then duplicate the time step
		if(iter<myTissue.max_iter/2 && time_step_doublings>0){
			std::cout<<"\n\n\nYESSSSSSS... next one with bigger time step\n\n\n";
			time_step = 2*time_step;
			time_step_doublings -= 1;
		}

		// ADVANCE IN TIME

		// nodal variables
		for(int nodei=0;nodei<myTissue.n_node;nodei++){
			if(myTissue.node_rho[nodei]>0){
				myTissue.node_rho_0[nodei] = myTissue.node_rho[nodei];
			}else{
				myTissue.node_rho_0[nodei] = 0.0;
			}
			if(myTissue.node_c[nodei]>0){
				myTissue.node_c_0[nodei] = myTissue.node_c[nodei];
			}else{
				myTissue.node_c_0[nodei] = 0.0;
			}
		}
		// integration point variables
		for(int elemi=0;elemi<myTissue.n_tri;elemi++){
			myTissue.ip_phic_0[elemi] = myTissue.ip_phic[elemi];
			myTissue.ip_mu_0[elemi] = myTissue.ip_mu[elemi];
			myTissue.ip_kc_0[elemi] = myTissue.ip_kc[elemi];
			myTissue.ip_a0c_0[elemi] = myTissue.ip_a0c[elemi];
			myTissue.ip_kappac_0[elemi] = myTissue.ip_kappac[elemi];
			//
			myTissue.ip_phif_0[elemi] = myTissue.ip_phif[elemi];
			myTissue.ip_kf_0[elemi] = myTissue.ip_kf[elemi];
			myTissue.ip_a0f_0[elemi] = myTissue.ip_a0f[elemi];
			myTissue.ip_kappaf_0[elemi] = myTissue.ip_kappaf[elemi];
			//
			myTissue.ip_lamdaP_0[elemi] = myTissue.ip_lamdaP[elemi];
		}

		// ADVANCE PRECURSOR IN TIME 
		int n_dof_p = myTissue.n_dof_p;
		std::cout<<"Advance precursor in time, solving for "<<n_dof_p<<" dof\n";
		VectorXd RR_p(n_dof_p);
		VectorXd SOL_p(n_dof_p);
		SparseMatrix<double, ColMajor> KKP(n_dof_p,n_dof_p);
		std::vector<T> KK_p_triplets;
		SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> >   solverP;
		// GLOBAL NEWTON-RAPHSON ITERATION
		int iter_p = 0;
		double residuum_p  = 1.;
		double residuum0_p = 1.;
		while(residuum_p>myTissue.tol && iter_p<myTissue.max_iter){
			// reset the solvers
			RR_p.setZero();
			SOL_p.setZero();
			KK_p_triplets.clear();
			Vector3d Re_p(3);
			Matrix3d Ke_p_p(3,3);
			// start loop over elements 
			int n_elem = myTissue.Tri.size();
			for(int ei=0;ei<n_elem;ei++){
			    // element stuff
			    // connectivity of the linear elements
			    std::vector<int> elem_ei = myTissue.Tri[ei];    
			    node_x_ni.clear();
				node_p_0_ni.clear();
				node_p_ni.clear();
				for(int ni=0;ni<3;ni++){
					// deformed positions
					node_x_ni.push_back(myTissue.node_x[elem_ei[ni]]);
					// precursor
					node_p_0_ni.push_back(myTissue.node_p_0[elem_ei[ni]]);
					node_p_ni.push_back(myTissue.node_p[elem_ei[ni]]);
				}
				Re_p.setZero();
				Ke_p_p.setZero();
				//*************************************************//
				evalPrec(time_step,node_p_0_ni,node_p_ni,node_x_ni, myTissue.ip_lamdaP[ei],myTissue.global_parameters,myTissue.elem_jac_IP[ei],Re_p,Ke_p_p);
				//*************************************************//
				// loop over nodes
				for(int nodei=0;nodei<3;nodei++){
					if(myTissue.dof_fwd_map_p[elem_ei[nodei]]>-1){
						RR_p(myTissue.dof_fwd_map_p[elem_ei[nodei]]) += Re_p(nodei);
						// tangent of the Precursor
						for(int nodej=0;nodej<3;nodej++){
							if(myTissue.dof_fwd_map_p[elem_ei[nodej]]>-1){
								T K_p_p_ni_nj = {myTissue.dof_fwd_map_p[elem_ei[nodei]],myTissue.dof_fwd_map_p[elem_ei[nodej]],Ke_p_p(nodei,nodej)};
								KK_p_triplets.push_back(K_p_p_ni_nj);
							}
						}
					}
				}
			}
			residuum_p=sqrt(RR_p.dot(RR_p));
			KKP.setFromTriplets(KK_p_triplets.begin(), KK_p_triplets.end());
			KKP.makeCompressed();
			solverP.compute(KKP);
			SOL_p = solverP.solve(-1.*RR_p);
			// update the solution
			for(int dofi=0;dofi<n_dof_p;dofi++){
				int dof_inv_i = myTissue.dof_inv_map_p[dofi];
				myTissue.node_p[dof_inv_i] += SOL_p(dofi);
			}
			iter_p += 1;
		}
		std::cout<<"converged precursor...\n";
		std::cout<<"End of P increments, iter_p: "<<iter_p<<", residual_p: "<<residuum_p<<"\n";
		for(int nodei=0;nodei<myTissue.n_node;nodei++){
			if(myTissue.node_p[nodei]>0){
				myTissue.node_p_0[nodei] = myTissue.node_p[nodei];
			}else{
				myTissue.node_p_0[nodei] = 0.0;
			}
		}

		time += time_step;
		myTissue.time = time;
		myTissue.time_step = time_step;
		if(time>time_final){break;}
		std::cout<<"End of Newton increments, iter: "<<iter<<", residual: "<<residuum<<"\nEnd of time step :"<<step<<", \nTime: "<<time<<"\n\n";
		

		// write out a paraview file.
		std::cout<<"check save "<<std::abs(time-save_step*save_freq)<<" < "<<time_step/2.0<<"?\n";
		if(std::abs(time-save_step*save_freq)<time_step/2.0){
			std::stringstream ss;
			ss << save_step ;
			std::string filename_step_Biol = filename + "_"+ss.str()+"_Biol.vtk";
			std::string filename_step_Mech = filename + "_"+ss.str()+"_Mech.vtk";
			std::string filename_step_tissue = filename +"_"+ ss.str()+".txt";
			writeParaviewBiol(myTissue,filename_step_Biol.c_str(),residuum);
			writeParaviewMech(myTissue,filename_step_Mech.c_str(),residuum);
			writeTissue(myTissue,filename_step_tissue.c_str(),time,residuum);
			std::cout<<"saved tissue for time "<<time<<" and save step "<<save_step<<"\n";
			save_step += 1;
		}

		// write out node variables in a file
		for(int nodei=0;nodei<save_node.size();nodei++){
			std::stringstream ss;
			ss << save_node[nodei];
			std::string filename_nodei = filename +"node"+ ss.str()+".txt";
			if(step==0){
				std::ofstream savefile(filename_nodei.c_str());
				if (!savefile) {throw std::runtime_error("Unable to open output file.");}
				savefile<<"## SAVING NODE "<<save_node[nodei]<<"TIME X(0) X(1) RHO C\n";
				savefile.close();
			}
			writeNode(myTissue,filename_nodei.c_str(),save_node[nodei],time);
		}
		// write out iP variables in a file
		for(int ipi=0;ipi<save_ip.size();ipi++){
			std::stringstream ss;
			ss << save_ip[ipi];
			std::string filename_ipi = filename + "IP"+ss.str()+".txt";
			if(step==0){
				std::ofstream savefile(filename_ipi.c_str());
				if (!savefile) {throw std::runtime_error("Unable to open output file.");}
				savefile<<"## SAVING IP "<<save_ip[ipi]<<"TIME phi a0(0) a0(1) kappa lamdaN(0) lamdaB(1)\n";
				savefile.close();
			}
			writeIP(myTissue,filename_ipi.c_str(),save_ip[ipi],time);
		}
	}
	// FINISH TIME LOOP
}

//---------------------------------------//
// SPARSE LOAD SOLVER
//---------------------------------------//
//
// NOTE:
// the mechanics part is highly non-linear so I'll use some load stepping
void sparseLoadSolver(tissue &myTissue, std::string filename)
{
	int n_dof = myTissue.n_dof_xx;
	std::cout<<"I will solve a small system of "<<n_dof<<" dof\n";
    VectorXd RR(n_dof);
	VectorXd SOL(n_dof);SOL.setZero();
    //SpMat KK(n_dof,n_dof); // sparse to solve with BiCG
    SparseMatrix<double, ColMajor> KK2(n_dof,n_dof);
	std::vector<T> KK_triplets;KK_triplets.clear();
	SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> >   solver2;
	// PARAMETERS FOR THE SIMULATION

	// Save an original configuration file
	std::stringstream ss;
	ss << "REFMECH";
	std::string filename_step_Biol = filename + ss.str()+"_Biol.vtk";
	std::string filename_step_Mech = filename + ss.str()+"_Mech.vtk";
	writeParaviewBiol(myTissue,filename_step_Biol.c_str(),0);
	writeParaviewMech(myTissue,filename_step_Mech.c_str(),0);

	//------------------------------------//
	// DECLARE VARIABLES YOU'LL USE LATER
	// deformed nodal positions for the element
	std::vector<Vector2d> node_x_ni;
	//
	// concentration and cells for previous and current time
	std::vector<double> node_rho_0_ni;
	std::vector<double> node_c_0_ni;
	//
	// values of the structural variables at the IP
	//
	double ip_phic_0_pi;
	double ip_mu_0_pi;
	double ip_kc_0_pi;
	Vector2d ip_a0c_0_pi;
	double ip_kappac_0_pi;
	//
	double ip_phif_0_pi;
	double ip_kf_0_pi;
	Vector2d ip_a0f_0_pi;
	double ip_kappaf_0_pi;
	//
	Vector2d ip_lamdaP_0_pi;
	//
	//
    // pieces of the Residuals
    VectorXd Re_x(6);
	//
    // pieces of the Tangents
	MatrixXd Ke_x_x(6,6);
	//
	// strain and stress tensors
	Matrix2d ip_FF, ip_sig_all;
	//
	//------------------------------------//


	// LOOP OVER TIME
	std::cout<<"start load stepping\n";
	double load_final = 1.0;
	double load_step  = myTissue.load_step;
	double load = 0.999999;
	int max_load_step_doublings = myTissue.max_load_step_doublings;
	double min_load_step = load_step/pow(2,max_load_step_doublings);
	int max_steps = 10000;
	int load_step_doublings = 0;
	double flag_res = 1;
	double flag_res0 = 1;
	double flag_incr = 1;
	double flag_normRR = 1;

	for(int step=0;step<max_steps;step++)
	{
		// GLOBAL NEWTON-RAPHSON ITERATION
		int iter = 0;
		double residuum  = 1.;
		double residuum0 = 1.;

		// Apply the step of the eBC_x
		std::map<int,double>::iterator it;
		for ( it = myTissue.eBC_x.begin(); it != myTissue.eBC_x.end(); it++ ){
			int dof_ind = it->first;
			double dof_u = it->second;
			int dof_node = dof_ind/2;
			int dof_coord = dof_ind%2;
    		myTissue.node_x[dof_node](dof_coord) =   myTissue.node_X[dof_node](dof_coord) + load*dof_u;
		}

		// create a copy of the nodal positions
		std::vector<Vector2d> node_x_copy = myTissue.node_x;

		std::cout<<"LOAD = "<<load<<", starting Newton iterations\n";
		//std::cout<<"tissue tolerance: "<<myTissue.tol<<"\n";
		//std::cout<<"max iterations: "<<myTissue.max_iter<<"\n";
		while(residuum>myTissue.tol && iter<myTissue.max_iter)
		{
			// reset the solvers
			KK2.setZero();
			RR.setZero();
			KK_triplets.clear();
			SOL.setZero();

			// START LOOP OVER ELEMENTS
			int n_elem = myTissue.Tri.size();
			for(int ei=0;ei<n_elem;ei++)
			{
		      	// element stuff
		      	//std::cout<<"Load element "<<ei<<"\n";
		      	// connectivity of the linear elements
		      	std::vector<int> elem_ei = myTissue.Tri[ei];
		      	//std::cout<<"element "<<ei<<": "<<elem_ei[0]<<","<<elem_ei[1]<<","<<elem_ei[2]<<","<<elem_ei[3]<<"\n";
			
				// deformed nodal positions for this element
				node_x_ni.clear();

				// concentration and cells for previous and current time
				node_rho_0_ni.clear();
				node_c_0_ni.clear();

				//
				for(int ni=0;ni<3;ni++){
					// deformed positions
					node_x_ni.push_back(myTissue.node_x[elem_ei[ni]]);
					//std::cout<<"pushing node "<<elem_ei[ni]<<": "<<node_x_ni[ni](0)<<","<<node_x_ni[ni](1)<<"\n";
					// cells and chemical
					node_rho_0_ni.push_back(myTissue.node_rho_0[elem_ei[ni]]);
					node_c_0_ni.push_back(myTissue.node_c_0[elem_ei[ni]]);
				}
				// single IP data
				ip_phic_0_pi = myTissue.ip_phic_0[ei];
				ip_mu_0_pi= myTissue.ip_mu_0[ei];
				ip_kc_0_pi= myTissue.ip_kc_0[ei];
				ip_a0c_0_pi= myTissue.ip_a0c_0[ei];
				ip_kappac_0_pi= myTissue.ip_kappac_0[ei];
				//
				ip_phif_0_pi = myTissue.ip_phif_0[ei];
				ip_kf_0_pi= myTissue.ip_kf_0[ei];
				ip_a0f_0_pi= myTissue.ip_a0f_0[ei];
				ip_kappaf_0_pi= myTissue.ip_kappaf_0[ei];
				//
				ip_lamdaP_0_pi= myTissue.ip_lamdaP_0[ei];
		      	// and calculate the element Re and Ke
		      	// pieces of the Residuals
		      	Re_x.setZero();

		      	// pieces of the Tangents
				Ke_x_x.setZero();

				// strains and stresses
				ip_FF.setZero();
				ip_sig_all.setZero();

		      	// subroutines to evaluate the element
		      	//
		      	//std::cout<<"wound at element "<<ei<<"\n";
		      	evalMechanics(
					load_step, load,
					myTissue.elem_jac_IP[ei],
					myTissue.global_parameters,myTissue.local_parameters,
					node_rho_0_ni,node_c_0_ni,
					ip_phic_0_pi,ip_mu_0_pi,ip_kc_0_pi,ip_a0c_0_pi,ip_kappac_0_pi,
					ip_phif_0_pi,ip_kf_0_pi,ip_a0f_0_pi,ip_kappaf_0_pi,
					ip_lamdaP_0_pi,
					node_x_ni,
					Re_x, Ke_x_x,
					ip_FF, ip_sig_all);				

		      	// assemble into KK triplets array and RR

		      	// write strains and stresses
		      	// NOTE: these get overwritten, so only converged values are really stored!!!
				myTissue.ip_FF[ei] = ip_FF;
				myTissue.ip_sig_all[ei] = ip_sig_all;

		      	// LOOP OVER NODES
				for(int nodei=0;nodei<3;nodei++){
					// ASSEMBLE DISPLACEMENT RESIDUAL AND TANGENTS
					for(int coordi=0;coordi<2;coordi++){
						if(myTissue.dof_fwd_map_xx[elem_ei[nodei]*2+coordi]>-1){

							// residual
							RR(myTissue.dof_fwd_map_xx[elem_ei[nodei]*2+coordi]) += Re_x(nodei*2+coordi);
							// loop over displacement dof for the tangent
							for(int nodej=0;nodej<3;nodej++){
								for(int coordj=0;coordj<2;coordj++){
									if(myTissue.dof_fwd_map_xx[elem_ei[nodej]*2+coordj]>-1){
										T K_x_x_nici_njcj = {myTissue.dof_fwd_map_xx[elem_ei[nodei]*2+coordi],myTissue.dof_fwd_map_xx[elem_ei[nodej]*2+coordj],Ke_x_x(nodei*2+coordi,nodej*2+coordj)};
										KK_triplets.push_back(K_x_x_nici_njcj);
									}
								}
							}
						}
					}
				}
				// FINISH LOOP OVER NODES (for assembly)
			}
			// FINISH LOOP OVER ELEMENTS

			// residual norm
			double normRR = sqrt(RR.dot(RR))/n_dof;
			//std::cout<<"RR\n"<<RR<<"\n";
			//break;
			if(iter==0){
				//std::cout<<"first residual\n"<<RR<<"\nRe_rho\n"<<Re_rho<<"\nRe_c\n"<<Re_c<<"\n";
				residuum0 = normRR;
				flag_res0 = residuum0;
				if(residuum0<myTissue.tol){std::cout<<"no need to solve?: "<<residuum0<<"\n";break;}
				//std::cout<<"first tangents\nKe_x_x\n"<<Ke_x_x<<"\nKe_x_rho\n"<<Ke_x_rho<<"\nKe_x_c\n"<<Ke_x_c<<"\n";
				//std::cout<<"first tangents\nKe_rho_x\n"<<Ke_rho_x<<"\nKe_rho_rho\n"<<Ke_rho_rho<<"\nKe_rho_c\n"<<Ke_rho_c<<"\n";
				//std::cout<<"first tangents\nKe_c_x\n"<<Ke_c_x<<"\nKe_c_rho\n"<<Ke_c_rho<<"\nKe_c_c\n"<<Ke_c_c<<"\n";
			}
			else{residuum = normRR/(1+residuum0);}
			flag_res = residuum;
			flag_normRR = normRR;

			// SOLVE: one approach
			//std::cout<<"solve\n";
			//KK.setFromTriplets(KK_triplets.begin(), KK_triplets.end());
			KK2.setFromTriplets(KK_triplets.begin(), KK_triplets.end());
			KK2.makeCompressed();
			//std::cout<<"KK2\n"<<KK2<<"\n";
			//solver2.analyzePattern(KK2);
			// Compute the numerical factorization
			//solver2.factorize(KK2);
			solver2.compute(KK2);
			//Use the factors to solve the linear system
			SOL = solver2.solve(-1.*RR);

			// SOLVE: alternate
			//BiCGSTAB<SparseMatrix<double> > solver;
			//solver.compute(KK2);
			//VectorXd SOL = solver.solve(-RR);
			//std::cout << "#iterations:     " << solver.iterations() << std::endl;
  			//std::cout << "estimated error: " << solver.error()      << std::endl;
			//std::cout<<SOL2<<"\n";

			// update the solution
			double normSOL = sqrt(SOL.dot(SOL))/n_dof;
			flag_incr = normSOL;
			residuum = std::min(residuum,normRR);
			residuum = std::min(residuum,normSOL);
			for(int dofi=0;dofi<n_dof;dofi++)
			{
				int dof_inv_i = myTissue.dof_inv_map_xx[dofi];
				int nodei  = dof_inv_i/2;
				int coordi = dof_inv_i%2;
				myTissue.node_x[nodei](coordi)+=SOL(dofi);
			}
			iter += 1;
			if(iter == myTissue.max_iter){
				if(load_step_doublings<max_load_step_doublings){
					std::cout<<"\n\n\nNOOOOOOOOO... restart with smaller load step\n";
					iter = 0;
					residuum0 = 1.0;
					residuum = 1.0;
					load = load - load_step;
					load_step = 0.5*load_step;
					load = load+load_step;
					load_step_doublings += 1;
					myTissue.node_x = node_x_copy;
					std::cout<<"load: "<<load<<"\n\n\n";
				}else{
					std::cout<<"\n\n\nCRAAAAAAAAP... \n\n\n";
				}
			}
//			std::cout<<"End of iteration : "<<iter<<",\nResidual before increment: "<<normRR<<"\nIncrement norm: "<<normSOL<<"\n\n";
			std::cout<<"End of iteration : "<<iter<<",\nCurrent value of the residual: "<<residuum<<"\n\n";
		}
		// FINISH WHILE LOOP OF NEWTON INCREMENTS
		// check if it converged fast then duplicate the time step

		load += load_step;
		myTissue.load = load;
		myTissue.load_step = load_step;

//		std::cout<<"End of Newton increments, iter: "<<iter<<", residual: "<<flag_normRR<<"\nEnd of load step :"<<load_step<<", \nLoad: "<<load<<"\n\n";
		std::cout<<"End of Newton increments, iter: "<<iter<<", residual: "<<residuum<<"\nEnd of load step :"<<load_step<<", \nLoad: "<<load<<"\n\n";
		if(load>=load_final){load=1; myTissue.load = 1; break;}

	}
	// FINISH LOAD LOOP
	std::stringstream ss2;
	ss2 << "LOAD" ;
	filename_step_Biol = filename + ss2.str()+"_Biol.vtk";
	filename_step_Mech = filename + ss2.str()+"_Mech.vtk";
	std::string filename_step_tissue = filename + ss2.str()+".txt";
	writeParaviewBiol(myTissue,filename_step_Biol.c_str(),flag_res);
	writeParaviewMech(myTissue,filename_step_Mech.c_str(),flag_res);
	writeTissue(myTissue,filename_step_tissue.c_str(),load,flag_res);

}



//---------------------------------------//
// PURE MECHANICS EQULIBRATION
//---------------------------------------//
//
void equilMechanics(tissue &myTissue, std::string filename)
{
	int n_dof = myTissue.n_dof_xx;
	std::cout<<"I will solve a small system of "<<n_dof<<" dof\n";
    VectorXd RR(n_dof);
	VectorXd SOL(n_dof);SOL.setZero();
    //SpMat KK(n_dof,n_dof); // sparse to solve with BiCG
    SparseMatrix<double, ColMajor> KK2(n_dof,n_dof);
	std::vector<T> KK_triplets;KK_triplets.clear();
	SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> >   solver2;
	// PARAMETERS FOR THE SIMULATION

	// Save an original configuration file
	std::stringstream ss;
	ss << "REFMECH";
	std::string filename_step_Biol = filename + ss.str()+"_Biol.vtk";
	std::string filename_step_Mech = filename + ss.str()+"_Mech.vtk";
	std::cout<<"write paraview\n";
	writeParaviewBiol(myTissue,filename_step_Biol.c_str(),0);
	writeParaviewMech(myTissue,filename_step_Mech.c_str(),0);
	std::cout<<"declare variables for the solver\n";

	//------------------------------------//
	// DECLARE VARIABLES YOU'LL USE LATER
	// deformed nodal positions for the element
	std::vector<Vector2d> node_x_ni;
	//
	// concentration and cells for previous and current time
	std::vector<double> node_rho_0_ni;
	std::vector<double> node_c_0_ni;
	//
	// values of the structural variables at the IP
	//
	double ip_phic_0_pi;
	double ip_mu_0_pi;
	double ip_kc_0_pi;
	Vector2d ip_a0c_0_pi;
	double ip_kappac_0_pi;
	//
	double ip_phif_0_pi;
	double ip_kf_0_pi;
	Vector2d ip_a0f_0_pi;
	double ip_kappaf_0_pi;
	//
	Vector2d ip_lamdaP_0_pi;
	//
	//
    // pieces of the Residuals
    VectorXd Re_x(6);
	//
    // pieces of the Tangents
	MatrixXd Ke_x_x(6,6);
	//
	// strain and stress tensors
	Matrix2d ip_FF, ip_sig_all;
	//
	//------------------------------------//


	// LOOP OVER TIME
	// double load_final = 1.0;
	double load_step  = myTissue.load_step;
	double load = 1.0;
	double flag_res = 1;
	double flag_res0 = 1;
	double flag_incr = 1;
	double flag_normRR = 1;

	// GLOBAL NEWTON-RAPHSON ITERATION
	int iter = 0;
	double residuum  = 1.;
	double residuum0 = 1.;

	std::cout<<"LOAD = "<<load<<", starting Newton iterations\n";
	//std::cout<<"tissue tolerance: "<<myTissue.tol<<"\n";
	//std::cout<<"max iterations: "<<myTissue.max_iter<<"\n";
	while(residuum>myTissue.tol && iter<myTissue.max_iter){
		// reset the solvers
		KK2.setZero();
		RR.setZero();
		KK_triplets.clear();
		SOL.setZero();

		// START LOOP OVER ELEMENTS
		int n_elem = myTissue.Tri.size();
		for(int ei=0;ei<n_elem;ei++){
	      	// element stuff
	      	//std::cout<<"Load element "<<ei<<"\n";
	      	// connectivity of the linear elements
	      	std::vector<int> elem_ei = myTissue.Tri[ei];
	      	//std::cout<<"element "<<ei<<": "<<elem_ei[0]<<","<<elem_ei[1]<<","<<elem_ei[2]<<","<<elem_ei[3]<<"\n";
		
			// deformed nodal positions for this element
			node_x_ni.clear();

			// concentration and cells for previous and current time
			node_rho_0_ni.clear();
			node_c_0_ni.clear();

			//
			for(int ni=0;ni<3;ni++){
				// deformed positions
				node_x_ni.push_back(myTissue.node_x[elem_ei[ni]]);
				//std::cout<<"pushing node "<<elem_ei[ni]<<": "<<node_x_ni[ni](0)<<","<<node_x_ni[ni](1)<<"\n";
				// cells and chemical
				node_rho_0_ni.push_back(myTissue.node_rho_0[elem_ei[ni]]);
				node_c_0_ni.push_back(myTissue.node_c_0[elem_ei[ni]]);
			}
			// single IP data
			ip_phic_0_pi = myTissue.ip_phic_0[ei];
			ip_mu_0_pi= myTissue.ip_mu_0[ei];
			ip_kc_0_pi= myTissue.ip_kc_0[ei];
			ip_a0c_0_pi= myTissue.ip_a0c_0[ei];
			ip_kappac_0_pi= myTissue.ip_kappac_0[ei];
			//
			ip_phif_0_pi = myTissue.ip_phif_0[ei];
			ip_kf_0_pi= myTissue.ip_kf_0[ei];
			ip_a0f_0_pi= myTissue.ip_a0f_0[ei];
			ip_kappaf_0_pi= myTissue.ip_kappaf_0[ei];
			//
			ip_lamdaP_0_pi= myTissue.ip_lamdaP_0[ei];
	      	// and calculate the element Re and Ke
	      	// pieces of the Residuals
	      	Re_x.setZero();

	      	// pieces of the Tangents
			Ke_x_x.setZero();

			// strains and stresses
			ip_FF.setZero();
			ip_sig_all.setZero();

	      	// subroutines to evaluate the element
	      	//
	      	//std::cout<<"wound at element "<<ei<<"\n";
	      	evalMechanics(
				load_step, load,
				myTissue.elem_jac_IP[ei],
				myTissue.global_parameters,myTissue.local_parameters,
				node_rho_0_ni,node_c_0_ni,
				ip_phic_0_pi,ip_mu_0_pi,ip_kc_0_pi,ip_a0c_0_pi,ip_kappac_0_pi,
				ip_phif_0_pi,ip_kf_0_pi,ip_a0f_0_pi,ip_kappaf_0_pi,
				ip_lamdaP_0_pi,
				node_x_ni,
				Re_x, Ke_x_x,
				ip_FF, ip_sig_all);
	      	//std::cout<<"Re_X\n";
	      	//std::cout<<Re_x;
			//std::cout<<"Ke_x_x\n"<<Ke_x_x<<"\n";

	      	// assemble into KK triplets array and RR

	      	// write strains and stresses
	      	// NOTE: these get overwritten, so only converged values are really stored!!!
			myTissue.ip_FF[ei] = ip_FF;
			myTissue.ip_sig_all[ei] = ip_sig_all;

	      	// LOOP OVER NODES
			for(int nodei=0;nodei<3;nodei++){
				// ASSEMBLE DISPLACEMENT RESIDUAL AND TANGENTS
				for(int coordi=0;coordi<2;coordi++){
					if(myTissue.dof_fwd_map_xx[elem_ei[nodei]*2+coordi]>-1){
						// residual
						RR(myTissue.dof_fwd_map_xx[elem_ei[nodei]*2+coordi]) += Re_x(nodei*2+coordi);
						// loop over displacement dof for the tangent
						for(int nodej=0;nodej<3;nodej++){
							for(int coordj=0;coordj<2;coordj++){
								if(myTissue.dof_fwd_map_xx[elem_ei[nodej]*2+coordj]>-1){
									T K_x_x_nici_njcj = {myTissue.dof_fwd_map_xx[elem_ei[nodei]*2+coordi],myTissue.dof_fwd_map_xx[elem_ei[nodej]*2+coordj],Ke_x_x(nodei*2+coordi,nodej*2+coordj)};
									KK_triplets.push_back(K_x_x_nici_njcj);
								}
							}
						}
					}
				}
			}
			// FINISH LOOP OVER NODES (for assembly)
		}
		// FINISH LOOP OVER ELEMENTS

		// residual norm
		double normRR = sqrt(RR.dot(RR))/n_dof;
		//std::cout<<"RR\n"<<RR<<"\n";
		//break;
		if(iter==0){
			//std::cout<<"first residual\n"<<RR<<"\nRe_rho\n"<<Re_rho<<"\nRe_c\n"<<Re_c<<"\n";
			residuum0 = normRR;
			flag_res0 = residuum0;
			if(residuum0<myTissue.tol){std::cout<<"no need to solve?: "<<residuum0<<"\n";residuum = residuum0;break;}
			//std::cout<<"first tangents\nKe_x_x\n"<<Ke_x_x<<"\nKe_x_rho\n"<<Ke_x_rho<<"\nKe_x_c\n"<<Ke_x_c<<"\n";
			//std::cout<<"first tangents\nKe_rho_x\n"<<Ke_rho_x<<"\nKe_rho_rho\n"<<Ke_rho_rho<<"\nKe_rho_c\n"<<Ke_rho_c<<"\n";
			//std::cout<<"first tangents\nKe_c_x\n"<<Ke_c_x<<"\nKe_c_rho\n"<<Ke_c_rho<<"\nKe_c_c\n"<<Ke_c_c<<"\n";
		}
		else{residuum = normRR/(1+residuum0);}
		flag_res = residuum;
		flag_normRR = normRR;

		// SOLVE: one approach
		//std::cout<<"solve\n";
		//KK.setFromTriplets(KK_triplets.begin(), KK_triplets.end());
		KK2.setFromTriplets(KK_triplets.begin(), KK_triplets.end());
		KK2.makeCompressed();
		//std::cout<<"KK2\n"<<KK2<<"\n";
		//solver2.analyzePattern(KK2);
		// Compute the numerical factorization
		//solver2.factorize(KK2);
		solver2.compute(KK2);
		//Use the factors to solve the linear system
		SOL = solver2.solve(-1.*RR);

		// SOLVE: alternate
		//BiCGSTAB<SparseMatrix<double> > solver;
		//solver.compute(KK2);
		//VectorXd SOL = solver.solve(-RR);
		//std::cout << "#iterations:     " << solver.iterations() << std::endl;
			//std::cout << "estimated error: " << solver.error()      << std::endl;
		//std::cout<<SOL2<<"\n";

		// update the solution
		double normSOL = sqrt(SOL.dot(SOL))/n_dof;
		flag_incr = normSOL;
		residuum = std::min(residuum,normRR);
		residuum = std::min(residuum,normSOL);
		for(int dofi=0;dofi<n_dof;dofi++){
			int dof_inv_i = myTissue.dof_inv_map_xx[dofi];
			int nodei  = dof_inv_i/2;
			int coordi = dof_inv_i%2;
			myTissue.node_x[nodei](coordi)+=SOL(dofi);
		}
		iter += 1;
//			std::cout<<"End of iteration : "<<iter<<",\nResidual before increment: "<<normRR<<"\nIncrement norm: "<<normSOL<<"\n\n";
		std::cout<<"End of iteration : "<<iter<<",\nCurrent value of the residual: "<<residuum<<"\n\n";
	}
	// FINISH WHILE LOOP OF NEWTON INCREMENTS
//		std::cout<<"End of Newton increments, iter: "<<iter<<", residual: "<<flag_normRR<<"\nEnd of load step :"<<load_step<<", \nLoad: "<<load<<"\n\n";
	std::cout<<"End of mechanics equilibration step, iter: "<<iter<<", residual: "<<residuum<<"\nEnd of load step :"<<load_step<<", \nLoad: "<<load<<"\n\n";

	// FINISH LOAD LOOP
	std::stringstream ss2;
	ss2 << "LOAD" ;
	filename_step_Biol = filename + ss2.str()+"_Biol.vtk";
	filename_step_Mech = filename + ss2.str()+"_Mech.vtk";
	std::string filename_step_tissue = filename + ss2.str()+".txt";
	writeParaviewBiol(myTissue,filename_step_Biol.c_str(),flag_res);
	writeParaviewMech(myTissue,filename_step_Mech.c_str(),flag_res);
	writeTissue(myTissue,filename_step_tissue.c_str(),load,flag_res);

}